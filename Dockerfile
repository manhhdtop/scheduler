ARG WORK_DIR=/home/gradle/project
ARG GRADLE_VERSION=8.7.0-jdk21
ARG JDK_VERSION=21-slim

FROM gradle:${GRADLE_VERSION} as builder
ARG WORK_DIR

WORKDIR ${WORK_DIR}

COPY build.gradle settings.gradle ./
COPY gradlew ./
COPY gradle/ ./gradle/

RUN gradle dependencies

COPY . .

RUN gradle bootJar -x test

FROM openjdk:${JDK_VERSION}
ARG WORK_DIR

WORKDIR /app

COPY --from=builder ${WORK_DIR}/build/libs/*.jar application.jar

CMD ["java", "-jar", "application.jar"]