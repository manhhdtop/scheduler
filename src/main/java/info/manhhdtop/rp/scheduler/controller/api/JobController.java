package info.manhhdtop.rp.scheduler.controller.api;

import info.manhhdtop.rp.scheduler.dto.request.job.CreateJobRequest;
import info.manhhdtop.rp.scheduler.dto.request.job.JobRequest;
import info.manhhdtop.rp.scheduler.dto.request.job.UpdateJobRequest;
import info.manhhdtop.rp.scheduler.dto.response.BaseResponse;
import info.manhhdtop.rp.scheduler.service.JobService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("api/job")
@RequiredArgsConstructor
@RestController
public class JobController {
    private final JobService jobService;

    @GetMapping
    public ResponseEntity<BaseResponse> getJobs(JobRequest request) {
        try {
            return ResponseEntity.ok(BaseResponse.success(jobService.getJobs(request)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(BaseResponse.error("Error when get job"));
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<BaseResponse> getJob(@PathVariable long id) {
        try {
            return ResponseEntity.ok(BaseResponse.success(jobService.getJob(id)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(BaseResponse.error("Error when get job"));
        }
    }

    @PostMapping
    public ResponseEntity<BaseResponse> createJob(@NotNull @RequestBody @Valid CreateJobRequest request) {
        try {
            return ResponseEntity.ok(jobService.createJob(request));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(BaseResponse.error("Error scheduling job"));
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<BaseResponse> updateJob(@PathVariable long id, @NotNull @RequestBody @Valid UpdateJobRequest request) {
        try {
            return ResponseEntity.ok(jobService.updateJob(id, request));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(BaseResponse.error("Error scheduling job"));
        }
    }

    @PostMapping("{id}/enable")
    public ResponseEntity<BaseResponse> enableJob(@PathVariable long id) {
        try {
            return ResponseEntity.ok(jobService.enableJob(id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(BaseResponse.error("Error scheduling job"));
        }
    }

    @PostMapping("{id}/disable")
    public ResponseEntity<BaseResponse> disableJob(@PathVariable long id) {
        try {
            return ResponseEntity.ok(jobService.disableJob(id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(BaseResponse.error("Error scheduling job"));
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<BaseResponse> deleteJob(@PathVariable long id) {
        try {
            return ResponseEntity.ok(jobService.deleteJob(id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(BaseResponse.error("Error scheduling job"));
        }
    }

}
