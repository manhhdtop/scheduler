package info.manhhdtop.rp.scheduler.controller;

import info.manhhdtop.rp.scheduler.dto.request.job.SchedulerListRequest;
import info.manhhdtop.rp.scheduler.service.SchedulerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("schedulers")
@RequiredArgsConstructor
@Slf4j
public class SchedulerController {
    private final SchedulerService schedulerService;

    @GetMapping
    public String list(
            SchedulerListRequest request,
            Pageable pageable,
            Model model) {
        var jobs = schedulerService.search(request, pageable);
        model.addAttribute("jobs", jobs);
        return "/scheduler/list";
    }
}
