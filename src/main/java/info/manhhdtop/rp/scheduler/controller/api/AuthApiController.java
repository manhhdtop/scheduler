package info.manhhdtop.rp.scheduler.controller.api;

import info.manhhdtop.rp.scheduler.dto.request.auth.LoginRequest;
import info.manhhdtop.rp.scheduler.dto.request.auth.RegisterRequest;
import info.manhhdtop.rp.scheduler.dto.response.BaseResponse;
import info.manhhdtop.rp.scheduler.service.AuthService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("api/auth")
@RequiredArgsConstructor
@RestController
@Slf4j
public class AuthApiController {
    private final AuthService authService;

    @PostMapping("login")
    public ResponseEntity<BaseResponse> login(@RequestBody @Valid LoginRequest request) {
        return ResponseEntity.ok(BaseResponse.success(authService.login(request)));
    }

    @PostMapping("register")
    public ResponseEntity<BaseResponse> register(@RequestBody @Valid RegisterRequest request) {
        return ResponseEntity.ok(BaseResponse.success(authService.register(request)));
    }
}
