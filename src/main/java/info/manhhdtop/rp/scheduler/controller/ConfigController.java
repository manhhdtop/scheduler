package info.manhhdtop.rp.scheduler.controller;

import info.manhhdtop.rp.scheduler.dto.request.config.ConfigSearchRequest;
import info.manhhdtop.rp.scheduler.dto.request.config.CreateConfigRequest;
import info.manhhdtop.rp.scheduler.enums.ConfigStatus;
import info.manhhdtop.rp.scheduler.enums.ConfigType;
import info.manhhdtop.rp.scheduler.exception.BadRequestException;
import info.manhhdtop.rp.scheduler.service.ConfigService;
import info.manhhdtop.rp.scheduler.utils.MessageUtil;
import info.manhhdtop.rp.scheduler.utils.PageUtil;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
@RequestMapping("config")
public class ConfigController {
    private final ConfigService configService;

    @GetMapping
    @PreAuthorize("hasAuthority('PERMISSION_CONFIG_VIEW')")
    public String index(Model model,
                        ConfigSearchRequest request,
                        @RequestParam(required = false) String method,
                        @RequestParam(required = false) String error,
                        Pageable pageable) {
        var page = configService.searchConfig(request, pageable);
        var configs = page.getContent();
        var totalPages = page.getTotalPages();
        var pageNumber = pageable.getPageNumber();
        var pageSize = pageable.getPageSize();
        var pages = PageUtil.pagination(pageNumber, totalPages);
        model.addAttribute("configs", configs);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("pageNumber", pageNumber);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("pages", pages);
        return "config/list";
    }

    @GetMapping("create")
    @PreAuthorize("hasAuthority('PERMISSION_CONFIG_EDIT')")
    public String create(
                      ConfigSearchRequest request,
                      @RequestParam(required = false) String method,
                      @RequestParam(required = false) String error,
                      Model model) {
        var types = ConfigType.names();
        var statuses = ConfigStatus.names();
        model.addAttribute("types", types);
        model.addAttribute("statuses", statuses);
        return "config/create_or_update";
    }

    @PostMapping("create")
    @PreAuthorize("hasAuthority('PERMISSION_CONFIG_EDIT')")
    public String create(@Valid CreateConfigRequest request, Model model, Pageable pageable) {
        try {
            configService.createConfig(request);
            return "redirect:/config";
        } catch (BadRequestException e) {
            model.addAttribute("errorMessage", e.getMessage());
        } catch (Exception e) {
            model.addAttribute("errorMessage", MessageUtil.getMessage("server.error.unknown"));
        }
        model.addAttribute("config", request);
        model.addAttribute("method", "CREATE");
        model.addAttribute("error", true);
        var types = ConfigType.names();
        var statuses = ConfigStatus.names();
        model.addAttribute("types", types);
        model.addAttribute("statuses", statuses);
        return "config/create_or_update";
    }

    @GetMapping("{id}")
    @PreAuthorize("hasAuthority('PERMISSION_CONFIG_EDIT')")
    public String update(Model model,
                      ConfigSearchRequest request,
                      @PathVariable Long id,
                      @RequestParam(required = false) String method,
                      @RequestParam(required = false) String error,
                      Pageable pageable) {
        var config = configService.getConfig(id);
        var types = ConfigType.names();
        var statuses = ConfigStatus.names();
        model.addAttribute("config", config);
        model.addAttribute("types", types);
        model.addAttribute("statuses", statuses);
        return "config/create_or_update";
    }
}
