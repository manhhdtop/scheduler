package info.manhhdtop.rp.scheduler.controller;

import info.manhhdtop.rp.scheduler.config.interceptor.ThymeleafLayoutInterceptor;
import info.manhhdtop.rp.scheduler.dto.request.auth.ChangePasswordRequest;
import info.manhhdtop.rp.scheduler.dto.request.auth.RegisterRequest;
import info.manhhdtop.rp.scheduler.service.AuthService;
import info.manhhdtop.rp.scheduler.utils.CookieUtil;
import info.manhhdtop.rp.scheduler.utils.RequestUtil;
import info.manhhdtop.rp.scheduler.utils.StringUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.stream.Stream;

@Controller
@RequestMapping("auth")
@RequiredArgsConstructor
@Slf4j
public class AuthController {
    private final AuthService authService;

    @GetMapping("change-password")
    public String changePassword(@RequestParam(value = "redirect_uri", required = false) String redirectUri, Model model) {
        var user = RequestUtil.getUser();
        if (StringUtil.isNotBlank(redirectUri)) {
            model.addAttribute("redirectUri", redirectUri);
        }
        model.addAttribute("username", user == null ? "" : user.getUsername());
        model.addAttribute(ThymeleafLayoutInterceptor.EMPTY_LAYOUT_ATTRIBUTE, true);
        return "auth/change-password";
    }

    @PostMapping("change-password")
    public String changePassword(@Valid ChangePasswordRequest request, Model model) {
        var user = RequestUtil.getUser();
        assert user != null;
        try {
            user = authService.changePassword(user.getId(), request);
            if (StringUtil.isNotBlank(request.getRedirectUri())) {
                return "redirect:" + request.getRedirectUri();
            }
            return "redirect:/";
        } catch (Exception e) {
            model.addAttribute("username", user.getUsername());
            model.addAttribute("error", e.getMessage());
        }
        model.addAttribute(ThymeleafLayoutInterceptor.EMPTY_LAYOUT_ATTRIBUTE, true);
        return "auth/change-password";
    }

    @GetMapping("login")
    public String login(@RequestParam(value = "error", required = false) String error,
                        @RequestParam(value = "username", required = false) String username,
                        @RequestParam(value = "redirectUrl", required = false) String redirectUrl,
                        HttpServletRequest httpServletRequest,
                        Model model) {
        if (RequestUtil.isLoggedIn()) {
            return StringUtil.isNotBlank(redirectUrl) ? "redirect:" + redirectUrl : "redirect:/";
        }
        if (StringUtil.isNotBlank(error)) {
            model.addAttribute("error", error);
        }
        if (StringUtil.isBlank(username)) {
            Stream.of(httpServletRequest.getCookies())
                    .filter(cookie -> cookie.getName().equals(CookieUtil.COOKIE_USERNAME))
                    .findFirst()
                    .ifPresent(cookieUsername -> model.addAttribute("username", cookieUsername.getValue()));
        } else {
            model.addAttribute("username", username);
        }
        model.addAttribute(ThymeleafLayoutInterceptor.EMPTY_LAYOUT_ATTRIBUTE, true);
        return "auth/login";
    }

    @GetMapping("register")
    public String register(RegisterRequest request, Model model) {
        model.addAttribute("request", request);
        model.addAttribute(ThymeleafLayoutInterceptor.EMPTY_LAYOUT_ATTRIBUTE, true);
        return "auth/register";
    }

    @PostMapping("register")
    public String register(Model model, @Valid RegisterRequest request) {
        try {
            authService.register(request);
            return "redirect:/auth/login";
        } catch (Exception ex) {
            model.addAttribute("request", request);
            model.addAttribute("error", ex.getMessage());
            model.addAttribute(ThymeleafLayoutInterceptor.EMPTY_LAYOUT_ATTRIBUTE, true);
            return "auth/register";
        }
    }
}
