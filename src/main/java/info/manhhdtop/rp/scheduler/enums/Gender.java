package info.manhhdtop.rp.scheduler.enums;

import info.manhhdtop.rp.scheduler.config.converter.GenericEnumConverter;
import jakarta.persistence.Converter;

public enum Gender {
    FEMALE,
    MALE,
    OTHER;

    @Converter(autoApply = true)
    public static class GenderConverter extends GenericEnumConverter<Gender> {
        public GenderConverter() {
            super(Gender.class);
        }
    }
}
