package info.manhhdtop.rp.scheduler.enums;

import info.manhhdtop.rp.scheduler.config.converter.GenericEnumConverter;
import jakarta.persistence.Converter;

public enum CommonStatus {
    ACTIVE,
    INACTIVE;

    @Converter(autoApply = true)
    public static class CommonStatusConverter extends GenericEnumConverter<CommonStatus> {
        public CommonStatusConverter() {
            super(CommonStatus.class);
        }
    }
}
