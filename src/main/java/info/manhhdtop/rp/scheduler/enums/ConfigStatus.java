package info.manhhdtop.rp.scheduler.enums;

import info.manhhdtop.rp.scheduler.config.converter.GenericEnumConverter;
import jakarta.persistence.Converter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum ConfigStatus {
    ACTIVE,
    INIT,
    DISABLE;

    public static List<String> names() {
        return Arrays.stream(values()).map(Enum::name).collect(Collectors.toList());
    }

    @Converter(autoApply = true)
    public static class ConfigStatusConverter extends GenericEnumConverter<ConfigStatus> {
        public ConfigStatusConverter() {
            super(ConfigStatus.class);
        }
    }
}
