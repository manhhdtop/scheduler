package info.manhhdtop.rp.scheduler.enums;

import lombok.Getter;

@Getter
public enum Bundle {
    DEFAULT("messages");

    private final String value;

    Bundle(String value) {
        this.value = value;
    }
}
