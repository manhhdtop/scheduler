package info.manhhdtop.rp.scheduler.enums;

import info.manhhdtop.rp.scheduler.config.converter.GenericEnumConverter;
import jakarta.persistence.Converter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum ConfigType {
    BOOLEAN,
    JSON,
    NUMBER,
    TEXT;

    public static List<String> names() {
        return Arrays.stream(values()).map(Enum::name).collect(Collectors.toList());
    }

    @Converter(autoApply = true)
    public static class CommonStatusConverter extends GenericEnumConverter<ConfigType> {
        public CommonStatusConverter() {
            super(ConfigType.class);
        }
    }
}
