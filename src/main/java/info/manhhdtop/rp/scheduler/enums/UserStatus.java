package info.manhhdtop.rp.scheduler.enums;

import info.manhhdtop.rp.scheduler.config.converter.GenericEnumConverter;
import jakarta.persistence.Converter;

public enum UserStatus {
    ACTIVE,
    CHANGE_PASSWORD,
    DISABLED,
    INACTIVE,
    LOCK,
    PENDING;

    public static UserStatus defaultRegisterStatus() {
        return ACTIVE;
    }

    @Converter(autoApply = true)
    public static class UserStatusConverter extends GenericEnumConverter<UserStatus> {
        public UserStatusConverter() {
            super(UserStatus.class);
        }
    }
}
