package info.manhhdtop.rp.scheduler.enums;

import info.manhhdtop.rp.scheduler.config.converter.GenericEnumConverter;
import jakarta.persistence.Converter;

public enum JobType {
    REST_API("RestApi");

    public final String name;

    JobType(String name) {
        this.name = name;
    }

    @Converter(autoApply = true)
    public static class JobTypeConverter extends GenericEnumConverter<JobType> {
        public JobTypeConverter() {
            super(JobType.class);
        }
    }
}
