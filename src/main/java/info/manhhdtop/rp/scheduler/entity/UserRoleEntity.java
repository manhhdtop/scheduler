package info.manhhdtop.rp.scheduler.entity;

import info.manhhdtop.rp.scheduler.entity.base.AuditModel;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLRestriction;

@AllArgsConstructor
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Table(
        name = "user_role",
        indexes = {
                @Index(columnList = "deleted"),
                @Index(columnList = "user_id,role_id,deleted", unique = true),
                @Index(columnList = "user_id"),
                @Index(columnList = "role_id"),
        }
)
@SQLRestriction("deleted = 0")
public class UserRoleEntity extends AuditModel {
    private Long userId;
    private Long roleId;
}
