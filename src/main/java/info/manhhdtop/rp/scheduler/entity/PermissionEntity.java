package info.manhhdtop.rp.scheduler.entity;

import info.manhhdtop.rp.scheduler.entity.base.AuditModel;
import info.manhhdtop.rp.scheduler.enums.CommonStatus;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLRestriction;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@Table(name = "permission")
@SQLRestriction("deleted=0")
public class PermissionEntity extends AuditModel {
    @Column(nullable = false, unique = true)
    private String code;
    private String name;
    private String description;
    private CommonStatus status;
}
