package info.manhhdtop.rp.scheduler.entity.base;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Data
@EqualsAndHashCode(callSuper = true)
@EntityListeners(AuditingEntityListener.class)
@MappedSuperclass
public class AuditModel extends BaseModel {
    @CreatedBy
    private Long createdBy;
    @Column(name = "updated_date")
    @LastModifiedDate
    private Long updatedDate;
    @LastModifiedBy
    private Long updatedBy;
}
