package info.manhhdtop.rp.scheduler.entity;

import info.manhhdtop.rp.scheduler.entity.base.AuditModel;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.Where;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@Table(
        name = "group_role",
        indexes = {
                @Index(columnList = "deleted"),
                @Index(columnList = "group_id,role_id,deleted", unique = true),
                @Index(columnList = "group_id"),
                @Index(columnList = "role_id"),
        }
)
@SQLRestriction("deleted = 0")
public class GroupRoleEntity extends AuditModel {
    @Column(name = "group_id")
    private String userId;
    @Column(name = "role_id")
    private String role_id;
}
