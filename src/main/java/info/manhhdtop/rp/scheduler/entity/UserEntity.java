package info.manhhdtop.rp.scheduler.entity;

import info.manhhdtop.rp.scheduler.dto.response.User;
import info.manhhdtop.rp.scheduler.entity.base.AuditModel;
import info.manhhdtop.rp.scheduler.enums.UserStatus;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLRestriction;

@AllArgsConstructor
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Table(
        name = "user",
        indexes = {
                @Index(columnList = "deleted"),
                @Index(columnList = "username,deleted", unique = true),
                @Index(columnList = "email,deleted", unique = true),
                @Index(columnList = "username"),
                @Index(columnList = "email"),
                @Index(columnList = "name"),
                @Index(columnList = "status"),
        }
)
@SQLRestriction("deleted = 0")
public class UserEntity extends AuditModel {
    @Column(name = "username", nullable = false, unique = true)
    private String username;
    @Column(name = "name")
    private String name;
    @Column(name = "password")
    private String password;
    @Column(name = "email", columnDefinition = "varchar(100)")
    private String email;
    @Column(name = "status", columnDefinition = "varchar(20)")
    private UserStatus status;

    public User toDto() {
        return User.builder()
                .username(username)
                .name(name)
                .password(password)
                .email(email)
                .status(status)
                .build();
    }

    public User toDto(ProfileEntity profileEntity) {
        return User.builder()
                .username(username)
                .name(name)
                .password(password)
                .email(email)
                .status(status)
                .profile(profileEntity.toDto())
                .build();
    }
}
