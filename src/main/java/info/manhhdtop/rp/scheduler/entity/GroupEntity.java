package info.manhhdtop.rp.scheduler.entity;

import info.manhhdtop.rp.scheduler.entity.base.AuditModel;
import info.manhhdtop.rp.scheduler.enums.CommonStatus;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLRestriction;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@Table(
        name = "_group",
        indexes = {
                @Index(columnList = "deleted"),
                @Index(columnList = "code,deleted", unique = true),
                @Index(columnList = "code"),
                @Index(columnList = "status"),
        }
)
@SQLRestriction("deleted=0")
public class GroupEntity extends AuditModel {
    @Column(name = "code", nullable = false, unique = true)
    private String code;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "status")
    private CommonStatus status;
}
