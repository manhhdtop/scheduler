package info.manhhdtop.rp.scheduler.entity;

import info.manhhdtop.rp.scheduler.dto.response.Config;
import info.manhhdtop.rp.scheduler.entity.base.AuditModel;
import info.manhhdtop.rp.scheduler.enums.ConfigStatus;
import info.manhhdtop.rp.scheduler.enums.ConfigType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLRestriction;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@Table(
        name = "config",
        indexes = {
                @Index(columnList = "deleted"),
                @Index(columnList = "code,deleted", unique = true),
                @Index(columnList = "code"),
        }
)
@SQLRestriction("deleted=0")
public class ConfigEntity extends AuditModel {
    private String code;
    private String name;
    @Column(name = "data", columnDefinition = "TEXT")
    private String data;
    @Column(name = "data_json", columnDefinition = "LONGTEXT")
    private String dataJson;
    private ConfigType type;
    private ConfigStatus status;

    public Config toDto() {
        return Config.builder()
                .id(getId())
                .code(code)
                .name(name)
                .data(data)
                .dataJson(dataJson)
                .type(type)
                .status(status)
                .build();
    }
}
