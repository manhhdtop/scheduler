package info.manhhdtop.rp.scheduler.entity;

import info.manhhdtop.rp.scheduler.entity.base.AuditModel;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLRestriction;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@Table(
        name = "role_permission",
        indexes = {
                @Index(columnList = "deleted"),
                @Index(columnList = "permission_id,role_id,deleted", unique = true),
                @Index(columnList = "permission_id"),
                @Index(columnList = "role_id"),
        }
)
@SQLRestriction("deleted = 0")
public class RolePermissionEntity extends AuditModel {
    private Long roleId;
    private Long permissionId;
}
