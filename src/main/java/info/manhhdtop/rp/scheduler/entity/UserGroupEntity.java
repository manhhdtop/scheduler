package info.manhhdtop.rp.scheduler.entity;

import info.manhhdtop.rp.scheduler.entity.base.AuditModel;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLRestriction;

@AllArgsConstructor
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Table(
        name = "user_group",
        indexes = {
                @Index(columnList = "deleted"),
                @Index(columnList = "user_id,group_id,deleted", unique = true),
                @Index(columnList = "user_id"),
                @Index(columnList = "group_id"),
        }
)
@SQLRestriction("deleted = 0")
public class UserGroupEntity extends AuditModel {
    private Long userId;
    private Long groupId;
}
