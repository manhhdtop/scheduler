package info.manhhdtop.rp.scheduler.entity;

import info.manhhdtop.rp.scheduler.dto.response.Profile;
import info.manhhdtop.rp.scheduler.entity.base.AuditModel;
import info.manhhdtop.rp.scheduler.enums.CommonStatus;
import info.manhhdtop.rp.scheduler.enums.Gender;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLRestriction;

@AllArgsConstructor
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Table(
        name = "profile",
        indexes = {
                @Index(columnList = "deleted"),
                @Index(columnList = "username,deleted", unique = true),
                @Index(columnList = "email,deleted", unique = true),
                @Index(columnList = "username"),
                @Index(columnList = "email"),
                @Index(columnList = "name"),
                @Index(columnList = "status"),
        }
)
@SQLRestriction("deleted = 0")
public class ProfileEntity extends AuditModel {
    @Column(nullable = false, unique = true)
    private Long userId;
    @Column(nullable = false, unique = true)
    private String username;
    private String name;
    private String email;
    private String phone;
    private String address;
    private Gender gender;
    private CommonStatus status;

    public Profile toDto() {
        return Profile.builder()
                .id(getId())
                .userId(userId)
                .username(username)
                .name(name)
                .phone(phone)
                .email(email)
                .address(address)
                .status(status)
                .build();
    }
}
