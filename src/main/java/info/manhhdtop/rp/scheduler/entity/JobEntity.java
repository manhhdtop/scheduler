package info.manhhdtop.rp.scheduler.entity;

import com.google.gson.reflect.TypeToken;
import info.manhhdtop.rp.scheduler.dto.request.job.CreateJobRequest;
import info.manhhdtop.rp.scheduler.dto.response.Job;
import info.manhhdtop.rp.scheduler.entity.base.AuditModel;
import info.manhhdtop.rp.scheduler.enums.JobType;
import info.manhhdtop.rp.scheduler.utils.JsonUtil;
import info.manhhdtop.rp.scheduler.utils.StringUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.*;
import org.hibernate.annotations.SQLRestriction;

import java.util.Map;

@AllArgsConstructor
@Builder
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Table(
        name = "job",
        indexes = {
                @Index(columnList = "deleted"),
                @Index(columnList = "_key,deleted", unique = true),
                @Index(columnList = "_key"),
                @Index(columnList = "type"),
                @Index(columnList = "name"),
        }
)
@SQLRestriction("deleted=0")
public class JobEntity extends AuditModel {
    private String name;
    @Column(name = "_key")
    private String key;
    private String description;
    private String cron;
    @Column(columnDefinition = "LONGTEXT")
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private String data;
    private boolean enabled;
    private JobType type;

    public static JobEntity of(CreateJobRequest request) {
        return JobEntity.builder()
                .name(request.getName())
                .key(StringUtil.createKey(request.getName()))
                .description(request.getDescription())
                .data(JsonUtil.toJson(request.getData()))
                .type(request.getType())
                .enabled(request.isEnabled())
                .cron(request.getCron())
                .build();
    }

    public Job toDto() {
        return Job.builder()
                .name(name)
                .cron(cron)
                .description(description)
                .data(getData())
                .type(type)
                .enabled(enabled)
                .build();
    }

    public Map<String, Object> getData() {
        if (data == null) {
            return null;
        }
        var type = new TypeToken<Map<String, Object>>() {
        }.getType();
        return JsonUtil.parse(data, type);
    }

    public void setData(Map<String, Object> data) {
        this.data = JsonUtil.toJson(data);
    }
}
