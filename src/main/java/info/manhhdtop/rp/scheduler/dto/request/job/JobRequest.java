package info.manhhdtop.rp.scheduler.dto.request.job;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Data;

@Data
public class JobRequest {
    private String name;
    @Min(1)
    @Max(5000)
    private int page = 1;
    @Min(10)
    @Max(500)
    private int size = 20;
}
