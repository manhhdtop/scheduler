package info.manhhdtop.rp.scheduler.dto.request.config;

import com.google.gson.JsonElement;
import info.manhhdtop.rp.scheduler.enums.ConfigStatus;
import info.manhhdtop.rp.scheduler.enums.ConfigType;
import info.manhhdtop.rp.scheduler.exception.BadRequestException;
import info.manhhdtop.rp.scheduler.utils.JsonUtil;
import info.manhhdtop.rp.scheduler.utils.StringUtil;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class CreateConfigRequest {
    @NotBlank
    @Size(min = 1, max = 255)
    private String code;
    @NotBlank
    @Size(min = 1, max = 255)
    private String name;
    @NotNull
    private ConfigType type;
    private String data;
    private String dataJson;
    private ConfigStatus status;

    public void processValue() {
        this.code = this.code.replaceAll("\\W", "").toUpperCase().strip();
        this.name = this.name.replaceAll("\\W", "").strip();
        if (this.type == ConfigType.JSON) {
            this.data = null;
            this.dataJson = JsonUtil.minify(this.dataJson);
        } else {
            this.data = StringUtil.strip(this.data);
            this.dataJson = null;
        }
        if (this.status == null) {
            this.status = ConfigStatus.ACTIVE;
        }
    }
}
