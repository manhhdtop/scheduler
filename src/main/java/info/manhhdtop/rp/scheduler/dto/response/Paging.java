package info.manhhdtop.rp.scheduler.dto.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Paging {
    private String name;
    private Integer page;
    private String url;
}
