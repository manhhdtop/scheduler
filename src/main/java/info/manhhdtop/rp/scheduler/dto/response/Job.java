package info.manhhdtop.rp.scheduler.dto.response;

import info.manhhdtop.rp.scheduler.dto.request.job.CreateJobRequest;
import info.manhhdtop.rp.scheduler.enums.JobType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class Job {
    private Long id;
    private String name;
    private String key;
    private String description;
    private String cron;
    private Map<String, Object> data;
    private boolean enabled;
    private JobType type;
    private Long createdDate;
    private Long createdBy;
    private Long updatedDate;
    private Long updatedBy;

    public static Job of(CreateJobRequest request) {
        return Job.builder()
                .name(request.getName())
                .description(request.getDescription())
                .data(request.getData())
                .type(request.getType())
                .enabled(request.isEnabled())
                .cron(request.getCron())
                .build();
    }
}
