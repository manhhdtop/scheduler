package info.manhhdtop.rp.scheduler.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import info.manhhdtop.rp.scheduler.enums.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class User {
    private Long id;
    private String username;
    private String name;
    @JsonIgnore
    private String password;
    private String email;
    private Profile profile;
    private UserStatus status;
}
