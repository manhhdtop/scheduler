package info.manhhdtop.rp.scheduler.dto.request.job;

import info.manhhdtop.rp.scheduler.constraint.ValidCronExpression;
import info.manhhdtop.rp.scheduler.enums.DataProcess;
import lombok.Data;

import java.util.Map;

@Data
public class UpdateJobRequest {
    private String description;
    private Map<String, Object> data;
    private DataProcess dataProcess = DataProcess.REPLACE;
    @ValidCronExpression
    private String cron;
    private boolean enabled;
}
