package info.manhhdtop.rp.scheduler.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class LoginResponse {
    private String name;
    private String token;
    private String tokenType;
    private String username;

    public static LoginResponse of(User user) {
        return LoginResponse.builder()
                .name(user.getName())
                .token(null)
                .tokenType(null)
                .username(user.getUsername())
                .build();
    }
}
