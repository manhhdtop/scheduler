package info.manhhdtop.rp.scheduler.dto.request.config;

import info.manhhdtop.rp.scheduler.enums.ConfigStatus;
import lombok.Data;

@Data
public class ConfigSearchRequest {
    private String code;
    private String name;
    private ConfigStatus status;
}
