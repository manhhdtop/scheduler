package info.manhhdtop.rp.scheduler.dto.request.auth;

import info.manhhdtop.rp.scheduler.enums.Gender;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class RegisterRequest {
    @NotBlank(message = "{auth.name.required}")
    @Size(min = 3, message = "{auth.name.invalid}")
    private String name;
    @NotBlank(message = "{auth.username.required}")
    @Pattern(regexp = "\\w{3,}", message = "{auth.username.invalid}")
    private String username;
    @NotBlank(message = "{auth.email.required}")
    @Email(message = "{auth.email.invalid}")
    private String email;
    @NotBlank(message = "{auth.password.required}")
    @Size(min = 6, message = "{auth.password.invalid}")
    private String password;
    private String phone;
    private String address;
    private Gender gender;
}
