package info.manhhdtop.rp.scheduler.dto.request.config;

import info.manhhdtop.rp.scheduler.enums.ConfigStatus;
import info.manhhdtop.rp.scheduler.enums.ConfigType;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class UpdateConfigRequest {
    @NotBlank
    @Size(min = 1, max = 255)
    private String name;
    @NotNull
    private ConfigType type;
    private String data;
    private String dataJson;
    private ConfigStatus status;
}
