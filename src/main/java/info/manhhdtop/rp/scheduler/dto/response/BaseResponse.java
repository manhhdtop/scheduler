package info.manhhdtop.rp.scheduler.dto.response;

import info.manhhdtop.rp.scheduler.utils.MessageUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class BaseResponse implements Serializable {
    public static final Integer STATUS_SUCCESS = 200;
    public static final Integer STATUS_BAD_REQUEST = 400;
    public static final Integer STATUS_NOT_FOUND = 404;
    private static final String MESSAGE_SUCCESS = MessageUtil.getMessage("response.success.message");

    private Integer status;
    private boolean success;
    private String message;
    private Object data;
    private Object optional;

    public static BaseResponse success() {
        return success(MESSAGE_SUCCESS, null, null);
    }

    public static BaseResponse success(Object data) {
        return success(MESSAGE_SUCCESS, data, null);
    }

    public static BaseResponse success(Object data, Object optional) {
        return success(MESSAGE_SUCCESS, data, optional);
    }

    public static BaseResponse success(String message, Object data) {
        return success(message, data, null);
    }

    public static BaseResponse success(String message, Object data, Object optional) {
        return BaseResponse.builder()
                .success(true)
                .status(STATUS_SUCCESS)
                .message(message)
                .data(data)
                .optional(optional)
                .build();
    }

    public static BaseResponse notFound(String message) {
        return error(STATUS_NOT_FOUND, message);
    }

    public static BaseResponse error(String message) {
        return error(STATUS_BAD_REQUEST, message);
    }

    public static BaseResponse error(Integer status, String message) {
        return error(status, message, null);
    }

    public static BaseResponse error(String message, Object data) {
        return error(STATUS_BAD_REQUEST, message, data);
    }

    public static BaseResponse error(Integer status, String message, Object data) {
        return BaseResponse.builder()
                .success(false)
                .status(status)
                .message(message)
                .data(data)
                .build();
    }

    public static BaseResponse exception() {
        return exception("Server error!!!");
    }

    public static BaseResponse exception(String message) {
        return BaseResponse.builder()
                .success(false)
                .status(500)
                .message(message)
                .build();
    }
}
