package info.manhhdtop.rp.scheduler.dto.request.job;

import lombok.Data;

@Data
public class SchedulerListRequest {
    private String name;
}
