package info.manhhdtop.rp.scheduler.dto.request.job;

import info.manhhdtop.rp.scheduler.constraint.JobConstrains;
import info.manhhdtop.rp.scheduler.constraint.ValidCronExpression;
import info.manhhdtop.rp.scheduler.enums.JobType;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.Map;

@Data
@JobConstrains
public class CreateJobRequest {
    @NotBlank
    @Length(min = 5, max = 255)
    private String name;
    @ValidCronExpression(required = true)
    private String cron;
    private Map<String, Object> data;
    private boolean enabled = true;
    private JobType type = JobType.REST_API;
    private String description;
}
