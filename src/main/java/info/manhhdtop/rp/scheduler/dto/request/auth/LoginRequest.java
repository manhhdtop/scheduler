package info.manhhdtop.rp.scheduler.dto.request.auth;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class LoginRequest {
    @NotBlank(message = "{auth.username.required}")
    @Pattern(regexp = "\\w{3,}", message = "{auth.username.invalid}")
    private String username;
    @NotBlank(message = "{auth.password.required}")
    @Size(min = 6, message = "{auth.password.invalid}")
    private String password;
}
