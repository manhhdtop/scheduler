package info.manhhdtop.rp.scheduler.dto.response;

import info.manhhdtop.rp.scheduler.enums.CommonStatus;
import info.manhhdtop.rp.scheduler.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class Profile {
    private Long id;
    private Long userId;
    private String username;
    private String name;
    private String email;
    private String phone;
    private String address;
    private Gender gender;
    private CommonStatus status;
}
