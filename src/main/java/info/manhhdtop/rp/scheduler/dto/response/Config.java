package info.manhhdtop.rp.scheduler.dto.response;

import com.fasterxml.jackson.core.type.TypeReference;
import info.manhhdtop.rp.scheduler.enums.ConfigStatus;
import info.manhhdtop.rp.scheduler.enums.ConfigType;
import info.manhhdtop.rp.scheduler.utils.MapperUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class Config {
    private Long id;
    private String code;
    private String name;
    private String data;
    private String dataJson;
    private ConfigType type;
    private ConfigStatus status;

    public Map<String, ?> getDataMap() {
        if (data == null) {
            return new HashMap<>();
        }
        var type = new TypeReference<Map<String, ?>>() {
        };
        return MapperUtil.getObjectMapper().convertValue(dataJson, type);
    }
}
