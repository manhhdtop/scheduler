package info.manhhdtop.rp.scheduler.config;

import jakarta.annotation.Nonnull;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private final String loginPageUrl;
    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    public CustomAuthenticationEntryPoint(String loginPageUrl) {
        this.loginPageUrl = loginPageUrl;
    }

    @Override
    public void commence(@Nonnull HttpServletRequest request, @Nonnull HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {

        String redirectUrl = loginPageUrl;
        String originalRequestUrl = request.getRequestURI();

        if (originalRequestUrl != null && !originalRequestUrl.isEmpty()) {
            String encodedUrl = URLEncoder.encode(originalRequestUrl, StandardCharsets.UTF_8);
            redirectUrl += "?redirectUrl=" + encodedUrl;
        }

        redirectStrategy.sendRedirect(request, response, redirectUrl);
    }
}
