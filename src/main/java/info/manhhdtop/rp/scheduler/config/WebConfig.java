package info.manhhdtop.rp.scheduler.config;

import info.manhhdtop.rp.scheduler.config.interceptor.ThymeleafLayoutInterceptor;
import info.manhhdtop.rp.scheduler.config.interceptor.UserStatusInterceptor;
import jakarta.annotation.Nonnull;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DelegatingWebMvcConfiguration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.thymeleaf.spring6.view.ThymeleafViewResolver;

@Configuration
@RequiredArgsConstructor
public class WebConfig extends DelegatingWebMvcConfiguration {
    private final LocaleChangeInterceptor localeChangeInterceptor;

    @Override
    public void addResourceHandlers(@Nonnull ResourceHandlerRegistry registry) {
        super.addResourceHandlers(registry);
        registry
                .addResourceHandler("/**")
                .addResourceLocations("classpath:/META-INF/resources/", "classpath:/resources/", "classpath:/static/", "classpath:/public/");
        registry.addResourceHandler("/webjars/**", "classpath:/META-INF/resources/webjars/");
    }

    @Override
    public void configureViewResolvers(@Nonnull ViewResolverRegistry registry) {
        super.configureViewResolvers(registry);
        var viewResolver = new ThymeleafViewResolver();
        viewResolver.setCache(false);
        registry.viewResolver(viewResolver);
    }

    @Override
    public void addInterceptors(@Nonnull InterceptorRegistry registry) {
        super.addInterceptors(registry);
        registry.addInterceptor(new ThymeleafLayoutInterceptor());
        registry.addInterceptor(new UserStatusInterceptor())
                .excludePathPatterns(
                        "/auth/**",
                        "/bootstrap-5.3.2/**",
                        "/css/**",
                        "/icon/**",
                        "/images/**",
                        "/js/**",
                        "/favicon**"
                );
        registry.addInterceptor(localeChangeInterceptor);
    }
}
