package info.manhhdtop.rp.scheduler.config;

import info.manhhdtop.rp.scheduler.entity.*;
import info.manhhdtop.rp.scheduler.enums.CommonStatus;
import info.manhhdtop.rp.scheduler.enums.ConfigStatus;
import info.manhhdtop.rp.scheduler.enums.UserStatus;
import info.manhhdtop.rp.scheduler.repository.*;
import info.manhhdtop.rp.scheduler.utils.SecurePasswordGenerator;
import info.manhhdtop.rp.scheduler.utils.constant.ConfigConstant;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.CollectionUtils;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class InitializeConfig {
    private final ConfigRepository configRepository;
    private final PasswordEncoder passwordEncoder;
    private final PermissionRepository permissionRepository;
    private final ProfileRepository profileRepository;
    private final RolePermissionRepository rolePermissionRepository;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;

    @Value("${application.password.generate.length:12}")
    private int generatePasswordLength;

    @PostConstruct
    public void initialize() {
        try {
            var applicationInitializationConfig = configRepository.findByCode(ConfigConstant.APPLICATION_INITIALIZATION);
            if (applicationInitializationConfig != null && Objects.equals(applicationInitializationConfig.getStatus(), ConfigStatus.ACTIVE)) {
                var roleAdmin = roleRepository.findByCode(ConfigConstant.DEFAULT_ADMIN_ROLE);
                var roleUser = roleRepository.findByCode(ConfigConstant.DEFAULT_ADMIN_ROLE);
                if (roleAdmin == null || roleUser == null) {
                    log.error("(initialize) DEFAULT ROLE IS NOT INITIALIZED");
                    System.exit(401);
                }
                DefaultRole.ADMIN_ROLE_ID = roleAdmin.getId();
                DefaultRole.USER_ROLE_ID = roleUser.getId();
                return;
            }
            log.info("(initialize) Initialize application configuration");
            if (applicationInitializationConfig == null) {
                applicationInitializationConfig = new ConfigEntity();
                applicationInitializationConfig.setCode(ConfigConstant.APPLICATION_INITIALIZATION);
                applicationInitializationConfig.setName("Application initialization");
                applicationInitializationConfig.setStatus(ConfigStatus.INIT);
                applicationInitializationConfig = configRepository.save(applicationInitializationConfig);
            }

            log.info("(initialize) Initialize default permission");
            // Permission config view
            var permissionConfigView = permissionRepository.findByCode("PERMISSION_CONFIG_VIEW");
            if (permissionConfigView == null) {
                permissionConfigView = new PermissionEntity();
                permissionConfigView.setCode("PERMISSION_CONFIG_VIEW");
                permissionConfigView.setName("Permission config view");
                permissionConfigView.setStatus(CommonStatus.ACTIVE);
                permissionConfigView = permissionRepository.save(permissionConfigView);
            }
            // Permission config create
            var permissionConfigCreate = permissionRepository.findByCode("PERMISSION_CONFIG_CREATE");
            if (permissionConfigCreate == null) {
                permissionConfigCreate = new PermissionEntity();
                permissionConfigCreate.setCode("PERMISSION_CONFIG_CREATE");
                permissionConfigCreate.setName("Permission config create");
                permissionConfigCreate.setStatus(CommonStatus.ACTIVE);
                permissionConfigCreate = permissionRepository.save(permissionConfigCreate);
            }
            // Permission role view
            var permissionRoleView = permissionRepository.findByCode("PERMISSION_ROLE_VIEW");
            if (permissionRoleView == null) {
                permissionRoleView = new PermissionEntity();
                permissionRoleView.setCode("PERMISSION_ROLE_VIEW");
                permissionRoleView.setName("Permission role view");
                permissionRoleView.setStatus(CommonStatus.ACTIVE);
                permissionRoleView = permissionRepository.save(permissionRoleView);
            }
            // Permission role create
            var permissionRoleCreate = permissionRepository.findByCode("PERMISSION_ROLE_CREATE");
            if (permissionRoleCreate == null) {
                permissionRoleCreate = new PermissionEntity();
                permissionRoleCreate.setCode("PERMISSION_ROLE_CREATE");
                permissionRoleCreate.setName("Permission role create");
                permissionRoleCreate.setStatus(CommonStatus.ACTIVE);
                permissionRoleCreate = permissionRepository.save(permissionRoleCreate);
            }
            // Permission scheduler view
            var permissionSchedulerView = permissionRepository.findByCode("PERMISSION_SCHEDULER_VIEW");
            if (permissionSchedulerView == null) {
                permissionSchedulerView = new PermissionEntity();
                permissionSchedulerView.setCode("PERMISSION_SCHEDULER_VIEW");
                permissionSchedulerView.setName("Permission scheduler view");
                permissionSchedulerView.setStatus(CommonStatus.ACTIVE);
                permissionSchedulerView = permissionRepository.save(permissionSchedulerView);
            }
            // Permission scheduler create
            var permissionSchedulerCreate = permissionRepository.findByCode("PERMISSION_SCHEDULER_CREATE");
            if (permissionSchedulerCreate == null) {
                permissionSchedulerCreate = new PermissionEntity();
                permissionSchedulerCreate.setCode("PERMISSION_SCHEDULER_CREATE");
                permissionSchedulerCreate.setName("Permission scheduler create");
                permissionSchedulerCreate.setStatus(CommonStatus.ACTIVE);
                permissionSchedulerCreate = permissionRepository.save(permissionSchedulerCreate);
            }
            // Permission user view
            var permissionUserView = permissionRepository.findByCode("PERMISSION_USER_VIEW");
            if (permissionUserView == null) {
                permissionUserView = new PermissionEntity();
                permissionUserView.setCode("PERMISSION_USER_VIEW");
                permissionUserView.setName("Permission user view");
                permissionUserView.setStatus(CommonStatus.ACTIVE);
                permissionUserView = permissionRepository.save(permissionUserView);
            }
            // Permission user create
            var permissionUserCreate = permissionRepository.findByCode("PERMISSION_USER_CREATE");
            if (permissionUserCreate == null) {
                permissionUserCreate = new PermissionEntity();
                permissionUserCreate.setCode("PERMISSION_USER_CREATE");
                permissionUserCreate.setName("Permission user create");
                permissionUserCreate.setStatus(CommonStatus.ACTIVE);
                permissionUserCreate = permissionRepository.save(permissionUserCreate);
            }
            log.info("(initialize) Initialize default role");
            var roleAdmin = roleRepository.findByCode(ConfigConstant.DEFAULT_ADMIN_ROLE);
            if (roleAdmin == null) {
                roleAdmin = new RoleEntity();
                roleAdmin.setCode(ConfigConstant.DEFAULT_ADMIN_ROLE);
                roleAdmin.setName("Role admin");
                roleAdmin.setStatus(CommonStatus.ACTIVE);
                roleAdmin = roleRepository.save(roleAdmin);
            }
            final var roleAdminId = roleAdmin.getId();
            var roleAdminPermissionIds = Set.of(
                    permissionConfigView.getId(), permissionConfigCreate.getId(),
                    permissionRoleView.getId(), permissionRoleCreate.getId(),
                    permissionUserView.getId(), permissionUserCreate.getId(),
                    permissionSchedulerView.getId(), permissionSchedulerCreate.getId()
            );

            final var roleAdminPermissions = rolePermissionRepository.findByRoleIdAndPermissionIdIn(roleAdminId, roleAdminPermissionIds);
            if (CollectionUtils.isEmpty(roleAdminPermissions) || roleAdminPermissions.size() < roleAdminPermissionIds.size()) {
                var newRoleAdminPermissions = roleAdminPermissionIds.stream()
                        .filter(e -> roleAdminPermissions.stream().noneMatch(f -> Objects.equals(f.getPermissionId(), e)))
                        .map(e -> {
                            var rolePermission = new RolePermissionEntity();
                            rolePermission.setRoleId(roleAdminId);
                            rolePermission.setPermissionId(e);
                            return rolePermission;
                        })
                        .collect(Collectors.toList());
                rolePermissionRepository.saveAll(newRoleAdminPermissions);
            }
            var roleUser = roleRepository.findByCode(ConfigConstant.DEFAULT_USER_ROLE);
            if (roleUser == null) {
                roleUser = new RoleEntity();
                roleUser.setCode(ConfigConstant.DEFAULT_USER_ROLE);
                roleUser.setName("Role user");
                roleUser.setStatus(CommonStatus.ACTIVE);
                roleUser = roleRepository.save(roleUser);
            }
            final var roleUserId = roleUser.getId();
            var roleUserPermissionIds = Set.of(
                    permissionSchedulerView.getId(), permissionSchedulerCreate.getId()
            );
            final var roleUserPermissions = rolePermissionRepository.findByRoleIdAndPermissionIdIn(roleAdminId, roleAdminPermissionIds);
            if (CollectionUtils.isEmpty(roleUserPermissions) || roleUserPermissions.size() < roleUserPermissionIds.size()) {
                var newRoleUserPermissions = roleUserPermissionIds.stream()
                        .filter(e -> roleUserPermissions.stream().noneMatch(f -> Objects.equals(f.getPermissionId(), e)))
                        .map(e -> {
                            var rolePermission = new RolePermissionEntity();
                            rolePermission.setRoleId(roleUserId);
                            rolePermission.setPermissionId(e);
                            return rolePermission;
                        })
                        .collect(Collectors.toSet());
                rolePermissionRepository.saveAll(newRoleUserPermissions);
            }

            var user = userRepository.findByUsername("admin").orElse(null);
            if (user == null) {
                user = new UserEntity();
                user.setUsername("admin");
                user.setName("Admin");
                var password = SecurePasswordGenerator.generateSecurePassword(generatePasswordLength);
                user.setPassword(passwordEncoder.encode(password));
                user.setEmail("admin@admin.com");
                user.setStatus(UserStatus.CHANGE_PASSWORD);
                user = userRepository.save(user);
                log.info("(initialize) Initialize admin user | default admin password: {}", password);
            }
            var profile = profileRepository.findByUserId(user.getId());
            if (profile == null) {
                profile = new ProfileEntity();
                profile.setUsername(user.getUsername());
                profile.setUserId(user.getId());
                profile.setName(user.getUsername());
                profile.setEmail(user.getEmail());
                profile.setStatus(CommonStatus.ACTIVE);
                profileRepository.save(profile);
            }
            var userRole = userRoleRepository.findByUserIdAndRoleId(user.getId(), roleAdminId);
            if (userRole == null) {
                userRole = new UserRoleEntity();
                userRole.setUserId(user.getId());
                userRole.setRoleId(roleAdminId);
                userRoleRepository.save(userRole);
            }

            DefaultRole.ADMIN_ROLE_ID = roleAdmin.getId();
            DefaultRole.USER_ROLE_ID = roleUser.getId();

            applicationInitializationConfig.setStatus(ConfigStatus.ACTIVE);
            configRepository.save(applicationInitializationConfig);
            log.info("(initialize) Initialize application configuration completed");
        } catch (Exception ex) {
            log.error("(initialize) Exception: {}", ex.getMessage(), ex);
        }
    }
}
