package info.manhhdtop.rp.scheduler.config;

import info.manhhdtop.rp.scheduler.utils.RequestUtil;
import info.manhhdtop.rp.scheduler.utils.StringUtil;
import info.manhhdtop.rp.scheduler.utils.constant.RequestConstant;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.security.web.session.RequestedUrlRedirectInvalidSessionStrategy;
import org.springframework.security.web.session.SimpleRedirectInvalidSessionStrategy;
import org.yaml.snakeyaml.util.UriEncoder;

import java.io.IOException;
import java.util.Set;

public class CustomInvalidSessionStrategy implements InvalidSessionStrategy {

    private final Set<String> permittedUrls;
    private final InvalidSessionStrategy requestedUrlRedirectStrategy;

    public CustomInvalidSessionStrategy(Set<String> permittedUrls) {
        this.permittedUrls = permittedUrls;
        this.requestedUrlRedirectStrategy = new RequestedUrlRedirectInvalidSessionStrategy();
    }

    @Override
    public void onInvalidSessionDetected(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (RequestUtil.isUriPermitted(request, permittedUrls)) {
            requestedUrlRedirectStrategy.onInvalidSessionDetected(request, response);
        } else {
            var redirectUrl = RequestConstant.LOGIN_URI;
            if (StringUtil.isNotBlank(request.getRequestURI()) && !request.getRequestURI().equals("/")) {
                var queryParams = UriEncoder.encode(String.format("%s=%s", RequestConstant.PARAM_REDIRECT_URL, request.getRequestURI()));
                redirectUrl = String.format("%s?%s", redirectUrl, queryParams);
            }
            var simpleRedirectStrategy = new SimpleRedirectInvalidSessionStrategy(redirectUrl);
            simpleRedirectStrategy.onInvalidSessionDetected(request, response);
        }
    }
}
