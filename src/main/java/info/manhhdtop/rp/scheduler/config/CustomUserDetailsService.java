package info.manhhdtop.rp.scheduler.config;

import info.manhhdtop.rp.scheduler.entity.RolePermissionEntity;
import info.manhhdtop.rp.scheduler.entity.UserRoleEntity;
import info.manhhdtop.rp.scheduler.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class CustomUserDetailsService implements UserDetailsService {
    private final PermissionRepository permissionRepository;
    private final RolePermissionRepository rolePermissionRepository;
    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));
        var roleIds = userRoleRepository.findByUserId(user.getId()).stream().map(UserRoleEntity::getRoleId).collect(Collectors.toSet());
        var permissionIds = rolePermissionRepository.findByRoleIdIn(roleIds).stream().map(RolePermissionEntity::getPermissionId).collect(Collectors.toSet());
        var permissions = permissionRepository.findAllById(permissionIds);
        return UserPrincipal.create(user, permissions);
    }

}
