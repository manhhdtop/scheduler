package info.manhhdtop.rp.scheduler.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import info.manhhdtop.rp.scheduler.entity.PermissionEntity;
import info.manhhdtop.rp.scheduler.entity.UserEntity;
import info.manhhdtop.rp.scheduler.enums.UserStatus;
import info.manhhdtop.rp.scheduler.utils.MapperUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class UserPrincipal implements UserDetails {
    private Long id;
    private String name;
    private String username;
    private String email;
    private UserStatus status;
    @JsonIgnore
    private String password;
    private List<PermissionEntity> permissions;

    private Collection<? extends GrantedAuthority> authorities;

    public static UserPrincipal create(UserEntity user) {
        var userPrincipal = MapperUtil.getModelMapper().map(user, UserPrincipal.class);
        userPrincipal.setAuthorities(List.of());

        return userPrincipal;
    }

    public static UserDetails create(UserEntity user, List<PermissionEntity> permissions) {
        var userPrincipal = MapperUtil.getModelMapper().map(user, UserPrincipal.class);
        var authorities = permissions.stream().map(e -> new SimpleGrantedAuthority(e.getCode())).collect(Collectors.toSet());
        userPrincipal.setAuthorities(authorities);
        userPrincipal.setPermissions(permissions);

        return userPrincipal;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}