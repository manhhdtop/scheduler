package info.manhhdtop.rp.scheduler.config.interceptor;

import info.manhhdtop.rp.scheduler.utils.MessageUtil;
import jakarta.annotation.Nonnull;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class ThymeleafLayoutInterceptor implements HandlerInterceptor {
    public static final String EMPTY_LAYOUT_ATTRIBUTE = "empty_layout";
    public static final String LAYOUT_TITLE = "layout_title";
    private static final String EMPTY_LAYOUT = "layout/empty";
    private static final String DEFAULT_LAYOUT = "layout/default";
    private static final String DEFAULT_VIEW_ATTRIBUTE_NAME = "view";
    private static final String REDIRECT_PREFIX = "redirect:";

    @Override
    public void postHandle(@Nonnull HttpServletRequest request, @Nonnull HttpServletResponse response, @Nonnull Object handler, ModelAndView modelAndView) throws Exception {
        if (modelAndView == null || !modelAndView.hasView()) {
            return;
        }
        String viewName = modelAndView.getViewName();
        if (viewName == null || viewName.startsWith(REDIRECT_PREFIX)) {
            return;
        }

        boolean isEmptyLayout = (boolean) modelAndView.getModel().getOrDefault(EMPTY_LAYOUT_ATTRIBUTE, false);

        if (isEmptyLayout) {
            modelAndView.setViewName(EMPTY_LAYOUT);
        } else {
            modelAndView.setViewName(DEFAULT_LAYOUT);
        }

        modelAndView.addObject(DEFAULT_VIEW_ATTRIBUTE_NAME, viewName);

        if (!modelAndView.getModel().containsKey(LAYOUT_TITLE)) {
            modelAndView.getModel().put(LAYOUT_TITLE, MessageUtil.getMessage("application.name"));
        }
    }
}
