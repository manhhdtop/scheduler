package info.manhhdtop.rp.scheduler.config;

import info.manhhdtop.rp.scheduler.utils.CookieUtil;
import info.manhhdtop.rp.scheduler.utils.StringUtil;
import info.manhhdtop.rp.scheduler.utils.constant.RequestConstant;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.yaml.snakeyaml.util.UriEncoder;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsService userDetailsService;

    @Value("${application.public-uri}")
    private Set<String> publicUri;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authenticationProvider(authenticationProvider());
        httpSecurity.authorizeHttpRequests(httpRequest -> {
                    publicUri = publicUri.stream()
                            .filter(StringUtil::isNotBlank)
                            .map(e -> e.strip().toLowerCase())
                            .collect(Collectors.toSet());
                    httpRequest.requestMatchers(publicUri.toArray(new String[0])).permitAll();
                    httpRequest.requestMatchers(HttpMethod.OPTIONS).permitAll()
                            .anyRequest().authenticated();
                }
        );
        httpSecurity.cors(AbstractHttpConfigurer::disable);
        httpSecurity.csrf(AbstractHttpConfigurer::disable);
        httpSecurity.exceptionHandling(e -> e.authenticationEntryPoint(new CustomAuthenticationEntryPoint(RequestConstant.LOGIN_URI)));
        httpSecurity.formLogin(f -> f.loginPage(RequestConstant.LOGIN_URI)
                .successHandler(authenticationSuccessHandler())
                .failureHandler(authenticationFailureHandler())
                .permitAll()
        );
        httpSecurity.logout(f -> f.logoutUrl(RequestConstant.LOGOUT_URI)
                .logoutSuccessUrl(RequestConstant.LOGIN_URI + "?logout=true")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .permitAll()
        );
        httpSecurity.httpBasic(Customizer.withDefaults());
        httpSecurity.sessionManagement(smc -> smc.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .invalidSessionStrategy(new CustomInvalidSessionStrategy(publicUri))
        );
        return httpSecurity.build();
    }

    @Bean
    public AuthenticationManager authenticationManager() {
        var authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder);

        return new ProviderManager(authenticationProvider);
    }

    private AuthenticationSuccessHandler authenticationSuccessHandler() {
        return new SavedRequestAwareAuthenticationSuccessHandler() {
            @Override
            public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
                var redirectUrl = request.getParameter(RequestConstant.PARAM_REDIRECT_URL);
                var rememberMe = request.getParameter(RequestConstant.PARAM_REMEMBER_ME);
                if (RequestConstant.DEFAULT_VALUE_REMEMBER_ME.equalsIgnoreCase(rememberMe)) {
                    CookieUtil.addCookie(response, CookieUtil.COOKIE_USERNAME, request.getParameter(RequestConstant.PARAM_USERNAME));
                } else {
                    CookieUtil.removeCookie(response, CookieUtil.COOKIE_USERNAME);
                }
                if (StringUtil.isBlank(redirectUrl)) {
                    super.onAuthenticationSuccess(request, response, authentication);
                } else {
                    getRedirectStrategy().sendRedirect(request, response, redirectUrl);
                }
            }
        };
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return (request, response, exception) -> {
            var username = request.getParameter("username");
            var error = UriEncoder.encode(exception.getMessage());
            var failureUrl = "/auth/login?error=" + error + (username != null ? "&username=" + username : "");
            response.sendRedirect(failureUrl);
        };
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        return new CustomAuthenticationProvider(passwordEncoder, userDetailsService);
    }
}
