package info.manhhdtop.rp.scheduler.config;

import info.manhhdtop.rp.scheduler.utils.RequestUtil;
import jakarta.annotation.Nonnull;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<Long> {

    @Override
    @Nonnull
    public Optional<Long> getCurrentAuditor() {
        var user = RequestUtil.getUser();
        if (user == null) {
            return Optional.empty();
        }

        return Optional.of(user.getId());
    }
}
