package info.manhhdtop.rp.scheduler.config.interceptor;

import info.manhhdtop.rp.scheduler.config.UserPrincipal;
import info.manhhdtop.rp.scheduler.enums.UserStatus;
import info.manhhdtop.rp.scheduler.utils.RequestUtil;
import jakarta.annotation.Nonnull;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.servlet.HandlerInterceptor;

import java.util.Objects;

public class UserStatusInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(@Nonnull HttpServletRequest request, @Nonnull HttpServletResponse response, @Nonnull Object handler) throws Exception {
        if (!RequestUtil.isLoggedIn()) {
            return true;
        }

        var principal = getCurrentUser();
        if (principal != null) {
            var status = principal.getStatus();
            var changePasswordUri = "/auth/change-password";
            if (UserStatus.CHANGE_PASSWORD.equals(status) && !Objects.equals(changePasswordUri, request.getRequestURI())) {
                var uri = request.getRequestURI();
                if (!uri.equals("/")) {
                    changePasswordUri = uri + "?redirect_uri=" + request.getRequestURI();
                }
                response.sendRedirect(changePasswordUri);
                return false;
            }
        }
        return true;
    }

    private UserPrincipal getCurrentUser() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        if (authentication.getPrincipal() instanceof UserDetails) {
            return (UserPrincipal) authentication.getPrincipal();
        }
        return null;
    }
}
