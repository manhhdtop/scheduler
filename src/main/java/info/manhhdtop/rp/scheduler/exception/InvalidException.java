package info.manhhdtop.rp.scheduler.exception;

import lombok.Getter;

@Getter
public class InvalidException extends RuntimeException {
    private final String error;

    public InvalidException(String error) {
        super(error);
        this.error = error;
    }
}
