package info.manhhdtop.rp.scheduler.exception;

import info.manhhdtop.rp.scheduler.utils.MessageUtil;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException implements Serializable {
    public BadRequestException(String message) {
        super(MessageUtil.getMessage(message));
    }

    public static BadRequestException build(String message) {
        return new BadRequestException(message);
    }
}
