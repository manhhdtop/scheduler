package info.manhhdtop.rp.scheduler.job;

import info.manhhdtop.rp.scheduler.enums.JobType;
import info.manhhdtop.rp.scheduler.exception.InvalidException;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@Slf4j
@SuppressWarnings({"unchecked", "unused"})
public class RestApiJob implements IJob, Serializable {
    private static final String KEY_BODY = "body";
    private static final String KEY_HEADERS = "headers";
    private static final String KEY_METHOD = "method";
    private static final String KEY_URL = "url";

    private static final List<HttpMethod> allowMethods = List.of(HttpMethod.DELETE, HttpMethod.GET, HttpMethod.POST, HttpMethod.PUT);

    private final RestTemplate restTemplate = new RestTemplate();

    @Override
    public JobDetail createJob(String name) {
        return createJob(name, null);
    }

    @Override
    public JobDetail createJob(String name, Map<String, Object> data) {
        if (CollectionUtils.isEmpty(data)) {
            return createJob(name, null);
        }
        var jobDataMap = new JobDataMap();
        jobDataMap.putAll(data);
        return createJob(name, jobDataMap);
    }

    @Override
    public JobDetail createJob(String name, JobDataMap jobDataMap) {
        return createJob(name, jobDataMap, null);
    }

    @Override
    public JobDetail createJob(String name, JobDataMap jobDataMap, String description) {
        return JobBuilder.newJob(RestApiJob.class)
                .withIdentity(name, JobType.REST_API.name)
                .usingJobData(jobDataMap)
                .storeDurably()
                .withDescription(description)
                .build();
    }

    @Override
    public void execute(JobExecutionContext context) {
        try {
            var data = context.getJobDetail().getJobDataMap();
            var url = data.getString(KEY_URL);
            log.info("(execute) url: {}", url);
            var headerMap = (Map<String, String>) data.get(KEY_HEADERS);
            var method = HttpMethod.valueOf((String) data.getOrDefault(KEY_METHOD, HttpMethod.GET));
            var body = data.getString(KEY_BODY);
            HttpEntity<?> httpEntity;
            MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.setAll(headerMap);
            if (CollectionUtils.isEmpty(headers)) {
                httpEntity = new HttpEntity<>(body);
            } else {
                httpEntity = new HttpEntity<>(body, headers);
            }
            restTemplate.exchange(url, method, httpEntity, String.class);
        } catch (Exception ex) {
            log.error("(RestApiJob.execute) Exception: {}", ex.getMessage());
        }
    }

    @Override
    public void validate(Map<String, Object> data) throws InvalidException {
        List<String> errorFields = new ArrayList<>();
        try {
            var url = (String) data.get(KEY_URL);
            var urlPattern = "^(https?|ftp)://([a-zA-Z0-9-_]+(:\\d{3,5})?)(\\.[a-zA-Z-_]{2,})*(/[a-zA-Z-_]+)*$";
            Pattern pattern = Pattern.compile(urlPattern);
            var matcher = pattern.matcher(url);
            if (!matcher.matches()) {
                errorFields.add("url");
            }
        } catch (Exception ex) {
            errorFields.add("url");
        }
        try {
            if (data.containsKey(KEY_HEADERS)) {
                var headers = (Map<String, String>) data.get(KEY_HEADERS);
            }
        } catch (Exception ex) {
            errorFields.add("headers");
        }
        try {
            if (data.containsKey(KEY_HEADERS)) {
                var method = HttpMethod.valueOf((String) data.getOrDefault(KEY_METHOD, "GET"));
                if (!allowMethods.contains(method)) {
                    errorFields.add("method");
                }
            }
        } catch (Exception ex) {
            errorFields.add("method");
        }
        try {
            if (data.containsKey(KEY_HEADERS)) {
                var body = data.get(KEY_BODY);
            }
        } catch (Exception ex) {
            errorFields.add("body");
        }

        if (!CollectionUtils.isEmpty(errorFields)) {
            throw new InvalidException("Validate failed. Field errors: [%s]".formatted(String.join(", ", errorFields)));
        }
    }
}
