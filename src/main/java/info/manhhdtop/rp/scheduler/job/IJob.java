package info.manhhdtop.rp.scheduler.job;

import info.manhhdtop.rp.scheduler.exception.InvalidException;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;

import java.util.Map;

public interface IJob extends Job {
    JobDetail createJob(String name);

    JobDetail createJob(String name, Map<String, Object> data);

    JobDetail createJob(String name, JobDataMap jobDataMap);

    JobDetail createJob(String name, JobDataMap jobDataMap, String description);

    void validate(Map<String, Object> data) throws InvalidException;
}
