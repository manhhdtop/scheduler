package info.manhhdtop.rp.scheduler.service;

import info.manhhdtop.rp.scheduler.dto.request.job.SchedulerListRequest;
import info.manhhdtop.rp.scheduler.dto.response.Job;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SchedulerService {
    List<Job> search(SchedulerListRequest request, Pageable pageable);
}
