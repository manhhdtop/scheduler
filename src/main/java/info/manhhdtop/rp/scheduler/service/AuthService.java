package info.manhhdtop.rp.scheduler.service;

import info.manhhdtop.rp.scheduler.dto.request.auth.ChangePasswordRequest;
import info.manhhdtop.rp.scheduler.dto.request.auth.LoginRequest;
import info.manhhdtop.rp.scheduler.dto.request.auth.RegisterRequest;
import info.manhhdtop.rp.scheduler.dto.response.LoginResponse;
import info.manhhdtop.rp.scheduler.dto.response.User;

public interface AuthService {
    User changePassword(Long userId, ChangePasswordRequest request);

    LoginResponse login(LoginRequest request);

    User register(RegisterRequest request);
}
