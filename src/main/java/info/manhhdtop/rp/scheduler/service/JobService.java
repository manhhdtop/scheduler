package info.manhhdtop.rp.scheduler.service;

import info.manhhdtop.rp.scheduler.dto.request.job.CreateJobRequest;
import info.manhhdtop.rp.scheduler.dto.request.job.JobRequest;
import info.manhhdtop.rp.scheduler.dto.request.job.UpdateJobRequest;
import info.manhhdtop.rp.scheduler.dto.response.BaseResponse;
import info.manhhdtop.rp.scheduler.entity.JobEntity;
import org.springframework.data.domain.Page;

public interface JobService {
    JobEntity getJob(long id);

    Page<JobEntity> getJobs(JobRequest request);

    BaseResponse createJob(CreateJobRequest request);

    BaseResponse enableJob(long id);

    BaseResponse disableJob(long id);

    BaseResponse deleteJob(long id);

    BaseResponse updateJob(long id, UpdateJobRequest request);
}
