package info.manhhdtop.rp.scheduler.service.impl;

import info.manhhdtop.rp.scheduler.dto.request.job.CreateJobRequest;
import info.manhhdtop.rp.scheduler.dto.request.job.JobRequest;
import info.manhhdtop.rp.scheduler.dto.request.job.UpdateJobRequest;
import info.manhhdtop.rp.scheduler.dto.response.BaseResponse;
import info.manhhdtop.rp.scheduler.entity.JobEntity;
import info.manhhdtop.rp.scheduler.enums.DataProcess;
import info.manhhdtop.rp.scheduler.enums.JobType;
import info.manhhdtop.rp.scheduler.repository.JobRepository;
import info.manhhdtop.rp.scheduler.service.JobService;
import info.manhhdtop.rp.scheduler.utils.JobUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;

@RequiredArgsConstructor
@Service
@Slf4j
public class JobServiceImpl implements JobService {
    private final JobRepository jobRepository;
    private final Scheduler scheduler;

    @Override
    public JobEntity getJob(long id) {
        return jobRepository.findById(id).orElse(null);
    }

    @Override
    public Page<JobEntity> getJobs(JobRequest request) {
        Pageable pageable = PageRequest.of(request.getPage() - 1, request.getSize());
        return jobRepository.findAll(pageable);
    }

    @Override
    public BaseResponse createJob(CreateJobRequest request) {
        try {
            JobEntity jobEntity = jobRepository.findByName(request.getName());
            if (jobEntity != null) {
                return BaseResponse.error("Job exist");
            }
            jobEntity = JobEntity.of(request);
            jobEntity = jobRepository.save(jobEntity);
            var jobDetail = JobUtils.createJob(request.getType(), jobEntity.getKey(), request.getData());
            var trigger = JobUtils.createTrigger(jobDetail, request.getCron());
            scheduler.scheduleJob(jobDetail, trigger);
            return BaseResponse.success();
        } catch (Exception ex) {
            log.error("(createJob) Exception: {}", ex.getMessage(), ex);
            return BaseResponse.error("Create job error!");
        }
    }

    @Override
    public BaseResponse enableJob(long id) {
        JobEntity jobEntity = jobRepository.findById(id).orElse(null);
        if (jobEntity == null) {
            return BaseResponse.error("Job not found");
        }
        if (jobEntity.isEnabled()) {
            return BaseResponse.success("Job is running!");
        }
        try {
            jobEntity.setEnabled(true);
            jobRepository.save(jobEntity);
            scheduler.resumeJob(JobKey.jobKey(jobEntity.getKey(), jobEntity.getType().name));
            return BaseResponse.success();
        } catch (Exception ex) {
            log.error("(enableJob) Exception: {}", ex.getMessage(), ex);
            return BaseResponse.error(ex.getMessage());
        }
    }

    @Override
    public BaseResponse disableJob(long id) {
        JobEntity jobEntity = jobRepository.findById(id).orElse(null);
        if (jobEntity == null) {
            return BaseResponse.error("Job not found");
        }
        if (!jobEntity.isEnabled()) {
            return BaseResponse.success("Job is not running!");
        }
        try {
            jobEntity.setEnabled(false);
            jobRepository.save(jobEntity);
            scheduler.pauseJob(JobKey.jobKey(jobEntity.getKey(), jobEntity.getType().name));
            return BaseResponse.success();
        } catch (Exception ex) {
            log.error("(enableJob) Exception: {}", ex.getMessage(), ex);
            return BaseResponse.error(ex.getMessage());
        }
    }

    @Override
    public BaseResponse deleteJob(long id) {
        JobEntity jobEntity = jobRepository.findById(id).orElse(null);
        if (jobEntity == null) {
            return BaseResponse.success("Job not found");
        }
        try {
            jobEntity.setDeleted(true);
            jobRepository.save(jobEntity);
            scheduler.deleteJob(JobKey.jobKey(jobEntity.getKey(), jobEntity.getType().name));
            return BaseResponse.success();
        } catch (Exception ex) {
            log.error("(enableJob) Exception: {}", ex.getMessage(), ex);
            return BaseResponse.success("Job not found");
        }
    }

    @Override
    public BaseResponse updateJob(long id, UpdateJobRequest request) {
        JobEntity jobEntity = jobRepository.findById(id).orElse(null);
        if (jobEntity == null) {
            return BaseResponse.error(404, "Job not found");
        }
        jobEntity.setData(request.getData());
        jobEntity.setDescription(request.getDescription());
        if (jobEntity.getType() == JobType.REST_API) {
            jobEntity.setCron(request.getCron());
        }
        jobEntity.setEnabled(request.isEnabled());
        jobRepository.save(jobEntity);
        try {
            var jobDetail = scheduler.getJobDetail(JobKey.jobKey(jobEntity.getKey(), jobEntity.getType().name));
            if (!CollectionUtils.isEmpty(request.getData())) {
                var data = jobDetail.getJobDataMap();
                if (request.getDataProcess() == DataProcess.REPLACE) {
                    data.clear();
                }
                data.putAll(request.getData());
            }
            var jobKey = JobKey.jobKey(jobEntity.getKey(), jobEntity.getType().name);
            var triggerKey = TriggerKey.triggerKey(jobEntity.getKey(), jobEntity.getType().name);
            CronTriggerImpl trigger = (CronTriggerImpl) scheduler.getTrigger(triggerKey);
            trigger.setCronExpression(jobEntity.getCron());
            scheduler.rescheduleJob(triggerKey, trigger);
            scheduler.deleteJob(jobKey);
            scheduler.scheduleJob(jobDetail, trigger);

            if (request.isEnabled()) {
                scheduler.resumeJob(jobKey);
                scheduler.resumeTrigger(triggerKey);
            } else {
                scheduler.pauseJob(jobKey);
                scheduler.pauseTrigger(triggerKey);
            }
        } catch (SchedulerException | ParseException e) {
            return BaseResponse.error(404, "Job not found");
        }
        return BaseResponse.success(jobEntity);
    }
}
