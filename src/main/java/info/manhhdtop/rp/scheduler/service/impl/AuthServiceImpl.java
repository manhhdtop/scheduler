package info.manhhdtop.rp.scheduler.service.impl;

import info.manhhdtop.rp.scheduler.config.DefaultRole;
import info.manhhdtop.rp.scheduler.dto.request.auth.ChangePasswordRequest;
import info.manhhdtop.rp.scheduler.dto.request.auth.LoginRequest;
import info.manhhdtop.rp.scheduler.dto.request.auth.RegisterRequest;
import info.manhhdtop.rp.scheduler.dto.response.LoginResponse;
import info.manhhdtop.rp.scheduler.dto.response.User;
import info.manhhdtop.rp.scheduler.entity.ProfileEntity;
import info.manhhdtop.rp.scheduler.entity.UserEntity;
import info.manhhdtop.rp.scheduler.entity.UserRoleEntity;
import info.manhhdtop.rp.scheduler.enums.CommonStatus;
import info.manhhdtop.rp.scheduler.enums.UserStatus;
import info.manhhdtop.rp.scheduler.exception.BadRequestException;
import info.manhhdtop.rp.scheduler.repository.*;
import info.manhhdtop.rp.scheduler.service.AuthService;
import info.manhhdtop.rp.scheduler.utils.RequestUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class AuthServiceImpl implements AuthService {
    private final AuthenticationManager authenticationManager;
    private final GroupRepository groupRepository;
    private final PasswordEncoder passwordEncoder;
    private final ProfileRepository profileRepository;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;

    @Override
    public User changePassword(Long userId, ChangePasswordRequest request) {
        if (request.getOldPassword().equals(request.getNewPassword())) {
            throw new BadRequestException("auth.new-password.used");
        }
        var userEntity = userRepository.findById(userId).orElseThrow(RuntimeException::new);
        if (!passwordEncoder.matches(request.getOldPassword(), userEntity.getPassword())) {
            throw new BadRequestException("auth.old-password.incorrect");
        }
        userEntity.setPassword(passwordEncoder.encode(request.getNewPassword()));
        userEntity.setStatus(UserStatus.ACTIVE);
        userEntity = userRepository.save(userEntity);
        RequestUtil.updateUserPrincipal(userEntity);
        return userEntity.toDto();
    }

    @Override
    public LoginResponse login(LoginRequest request) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        var entity = userRepository.findByUsername(request.getUsername());
        if (entity.isPresent()) {
            var user = entity.get().toDto();
            return LoginResponse.of(user);
        }
        return null;
    }

    @Override
    public User register(RegisterRequest request) {
        var exist = userRepository.checkUserExist(request.getUsername(), request.getEmail());
        if (exist) {
            throw new BadRequestException("auth.username.exist");
        }
        // create user
        var user = new UserEntity();
        user.setName(request.getName().strip());
        user.setUsername(request.getUsername().strip());
        user.setEmail(request.getEmail().strip());
        user.setPassword(passwordEncoder.encode(request.getPassword().strip()));
        user.setStatus(UserStatus.defaultRegisterStatus());
        user = userRepository.save(user);
        var profile = new ProfileEntity();
        profile.setUserId(user.getId());
        profile.setUsername(user.getUsername());
        profile.setEmail(user.getEmail());
        profile.setName(user.getName());
        profile.setAddress(request.getAddress().strip());
        profile.setGender(request.getGender());
        profile.setPhone(request.getPhone().strip());
        profile.setStatus(CommonStatus.ACTIVE);
        profile = profileRepository.save(profile);
        // apply role
        var userRole = userRoleRepository.findByUserIdAndRoleId(user.getId(), DefaultRole.USER_ROLE_ID);
        if (userRole != null) {
            userRole = new UserRoleEntity();
            userRole.setUserId(user.getId());
            userRole.setRoleId(DefaultRole.USER_ROLE_ID);
            userRoleRepository.save(userRole);
        }

        return user.toDto(profile);
    }
}
