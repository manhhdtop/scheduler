package info.manhhdtop.rp.scheduler.service;

import info.manhhdtop.rp.scheduler.dto.request.config.ConfigSearchRequest;
import info.manhhdtop.rp.scheduler.dto.request.config.CreateConfigRequest;
import info.manhhdtop.rp.scheduler.dto.request.config.UpdateConfigRequest;
import info.manhhdtop.rp.scheduler.dto.response.Config;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ConfigService {
    Config getConfig(Long id);

    Config getConfig(String code);

    Config createConfig(CreateConfigRequest request);

    Config updateConfig(UpdateConfigRequest request);

    Page<Config> searchConfig(ConfigSearchRequest request, Pageable pageable);
}
