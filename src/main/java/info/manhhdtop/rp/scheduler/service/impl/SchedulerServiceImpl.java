package info.manhhdtop.rp.scheduler.service.impl;

import info.manhhdtop.rp.scheduler.dto.request.job.SchedulerListRequest;
import info.manhhdtop.rp.scheduler.dto.response.Job;
import info.manhhdtop.rp.scheduler.service.SchedulerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
@Slf4j
public class SchedulerServiceImpl implements SchedulerService {
    @Override
    public List<Job> search(SchedulerListRequest request, Pageable pageable) {
        return List.of();
    }
}
