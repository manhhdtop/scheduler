package info.manhhdtop.rp.scheduler.service.impl;

import com.google.gson.JsonElement;
import info.manhhdtop.rp.scheduler.dto.request.config.ConfigSearchRequest;
import info.manhhdtop.rp.scheduler.dto.request.config.CreateConfigRequest;
import info.manhhdtop.rp.scheduler.dto.request.config.UpdateConfigRequest;
import info.manhhdtop.rp.scheduler.dto.response.Config;
import info.manhhdtop.rp.scheduler.entity.ConfigEntity;
import info.manhhdtop.rp.scheduler.exception.BadRequestException;
import info.manhhdtop.rp.scheduler.repository.ConfigRepository;
import info.manhhdtop.rp.scheduler.service.ConfigService;
import info.manhhdtop.rp.scheduler.utils.JsonUtil;
import info.manhhdtop.rp.scheduler.utils.MapperUtil;
import info.manhhdtop.rp.scheduler.utils.StringUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Slf4j
public class ConfigServiceImpl implements ConfigService {
    private final ConfigRepository configRepository;

    @Override
    public Config getConfig(Long id) {
        if (id == null) {
            return null;
        }
        var entity = configRepository.findById(id).orElse(null);
        if (entity == null) {
            return null;
        }
        return entity.toDto();
    }

    @Override
    public Config getConfig(String code) {
        if (StringUtil.isBlank(code)) {
            return null;
        }
        var entity = configRepository.findByCode(code.strip().toUpperCase());
        if (entity == null) {
            return null;
        }
        return entity.toDto();
    }

    @Override
    public Config createConfig(CreateConfigRequest request) {
        var exist = configRepository.findByCode(request.getCode());
        if (exist != null) {
            throw BadRequestException.build("config.exist");
        }
        switch (request.getType()) {
            case JSON -> {
                try {
                    JsonUtil.parse(request.getDataJson(), JsonElement.class);
                } catch (Exception ex) {
                    throw BadRequestException.build("config.json.invalid");
                }
            }
            case NUMBER -> {
                try {
                    Double.parseDouble(request.getData());
                } catch (Exception ex) {
                    throw BadRequestException.build("config.value.invalid");
                }
            }
            case BOOLEAN -> {
                try {
                    Boolean.parseBoolean(request.getData());
                } catch (Exception ex) {
                    throw BadRequestException.build("config.value.invalid");
                }
            }
        }
        request.processValue();
        var entity = MapperUtil.getModelMapper().map(request, ConfigEntity.class);
        entity = configRepository.save(entity);
        return entity.toDto();
    }

    @Override
    public Config updateConfig(UpdateConfigRequest request) {
        return null;
    }

    @Override
    public Page<Config> searchConfig(ConfigSearchRequest request, Pageable pageable) {
        var page = configRepository.searchConfig(request, pageable);
        if (page.isEmpty()) {
            return new PageImpl<>(new ArrayList<>(), pageable, 0L);
        }
        var configs = page.get()
                .map(ConfigEntity::toDto)
                .collect(Collectors.toList());
        return new PageImpl<>(configs, pageable, page.getTotalElements());
    }
}
