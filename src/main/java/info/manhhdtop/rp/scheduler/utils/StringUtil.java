package info.manhhdtop.rp.scheduler.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class StringUtil {
    public static boolean isBlank(String str) {
        return (str == null || str.isEmpty() || !containsText(str));
    }

    public static boolean isNotBlank(String str) {
        return (str != null && !str.isEmpty() && containsText(str));
    }

    public static boolean containsWhitespace(@Nullable CharSequence str) {
        if (!hasLength(str)) {
            return false;
        }

        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasLength(@Nullable CharSequence str) {
        return (str != null && !str.isEmpty());
    }

    public static boolean hasLength(@Nullable String str) {
        return (str != null && !str.isEmpty());
    }

    private static boolean containsText(CharSequence str) {
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static String strip(String str) {
        if (str == null) {
            return null;
        }
        return str.strip();
    }

    public static String stripNonNull(String str) {
        if (str == null) {
            return "";
        }
        return str.strip();
    }

    public static boolean startsWith(String token, String str) {
        return isBlank(token) || !token.startsWith(str);
    }

    public static boolean endsWith(String token, String str) {
        return isBlank(token) || !token.endsWith(str);
    }

    public static String base64Encode(@Nullable String str) {
        if (isBlank(str)) {
            return "";
        }
        return Base64.getEncoder().encodeToString(str.getBytes(StandardCharsets.UTF_8));
    }

    public static String base64Encode(@Nullable byte[] bytes) {
        if (bytes == null) {
            return "";
        }
        return Base64.getEncoder().encodeToString(bytes);
    }

    public static String base64Decode(@Nullable String str) {
        if (isBlank(str)) {
            return "";
        }
        return new String(Base64.getDecoder().decode(str), StandardCharsets.UTF_8);
    }

    public static String removeAccent(String s) {
        return removeAccent(s, false);
    }

    public static String removeAccent(String s, boolean isLowerCase) {
        if (isLowerCase) {
            s = s.toLowerCase();
        }

        s = s.replaceAll("đ", "d").trim();

        if (!isLowerCase) {
            s = s.replaceAll("Đ", "D");
        }
        return StringUtils.stripAccents(s);
    }

    public static String createKey(String name) {
        var key = removeAccent(name);
        key = key.replaceAll("\\s+", "_");
        return key.toUpperCase();
    }
}
