package info.manhhdtop.rp.scheduler.utils;

import info.manhhdtop.rp.scheduler.config.UserPrincipal;
import info.manhhdtop.rp.scheduler.dto.response.User;
import info.manhhdtop.rp.scheduler.entity.UserEntity;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.server.PathContainer;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.util.pattern.PathPattern;
import org.springframework.web.util.pattern.PathPatternParser;

import java.util.Set;

public class RequestUtil {
    public static User getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || !authentication.isAuthenticated() || authentication instanceof AnonymousAuthenticationToken) {
            return null;
        }

        var principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserPrincipal userPrincipal) {
            return User.builder()
                    .id(userPrincipal.getId())
                    .username(userPrincipal.getUsername())
                    .name(userPrincipal.getName())
                    .email(userPrincipal.getEmail())
                    .status(userPrincipal.getStatus())
                    .build();
        }
        return null;
    }

    public static boolean isLoggedIn() {
        final var authentication = SecurityContextHolder.getContext().getAuthentication();
        return null != authentication && !("anonymousUser").equals(authentication.getName());
    }

    public static boolean isUriPermitted(HttpServletRequest request, Set<String> uris) {
        final var parser = new PathPatternParser();
        var path = PathContainer.parsePath(request.getRequestURI());
        boolean anyMatch = false;
        for (String s : uris) {
            PathPattern pathPattern = parser.parse(s);
            if (pathPattern.matches(path)) {
                anyMatch = true;
                break;
            }
        }
        return anyMatch;
    }

    public static void updateUserPrincipal(UserEntity userEntity) {
        var principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == null) {
            return;
        }
        MapperUtil.getModelMapper().map(userEntity, principal);
    }
}
