package info.manhhdtop.rp.scheduler.utils.constant;

public class RequestConstant {
    public static final String LOGIN_URI = "/auth/login";
    public static final String LOGOUT_URI = "/auth/logout";
    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_REDIRECT_URL = "redirectUrl";
    public static final String PARAM_REMEMBER_ME = "rememberMe";
    public static final String DEFAULT_VALUE_REMEMBER_ME = "ON";
}
