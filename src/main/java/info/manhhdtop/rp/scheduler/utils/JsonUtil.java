package info.manhhdtop.rp.scheduler.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;

import java.lang.reflect.Type;

public class JsonUtil {
    @Getter
    private static final Gson gson;

    static {
        var gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        gson = gsonBuilder.create();
    }

    private JsonUtil() {
    }

    public static String toJson(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof String) {
            return (String) obj;
        }
        return gson.toJson(obj);
    }

    public static <T> T parse(String data, Class<T> tClass) {
        if (data == null) {
            return null;
        }
        return gson.fromJson(data, tClass);
    }

    public static <T> T parse(String data, Type type) {
        if (data == null) {
            return null;
        }
        return gson.fromJson(data, type);
    }

    public static String minify(String json) {
        try {
            JsonNode jsonNode = null;
            jsonNode = MapperUtil.getObjectMapper().readValue(json, JsonNode.class);
            return jsonNode.toString();
        } catch (JsonProcessingException e) {
            return null;
        }
    }
}
