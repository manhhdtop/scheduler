package info.manhhdtop.rp.scheduler.utils;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.stream.Stream;

public class CookieUtil {
    public static final String COOKIE_USERNAME = "username";
    private static final int MAX_AGE = 7 * 24 * 60 * 60;
    private static final String PATH = "/";

    public static Cookie getCookie(HttpServletRequest request, String name) {
        var cookies = request.getCookies();
        if (cookies == null || cookies.length == 0) {
            return null;
        }
        return Stream.of(cookies).filter(cookie -> cookie.getName().equals(name)).findFirst().orElse(null);
    }

    public static void addCookie(HttpServletResponse response, String name, String value) {
        addCookie(response, name, value, MAX_AGE);
    }

    public static void addCookie(HttpServletResponse response, String name, String value, int maxAge) {
        if (StringUtil.isBlank(name)) {
            return;
        }
        var cookie = new Cookie(name.strip().toLowerCase(), value);
        cookie.setMaxAge(maxAge); // 1 week
        cookie.setHttpOnly(true);
        cookie.setPath(PATH);
        response.addCookie(cookie);
    }

    public static void removeCookie(HttpServletResponse response, String name) {
        addCookie(response, name, null);
    }
}
