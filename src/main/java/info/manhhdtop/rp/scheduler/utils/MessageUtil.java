package info.manhhdtop.rp.scheduler.utils;

import info.manhhdtop.rp.scheduler.enums.Bundle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Locale;

@Slf4j
public class MessageUtil {
    public static final Locale locale = Locale.ROOT;
    public static final ReloadableResourceBundleMessageSource messageSource;

    static {
        messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setAlwaysUseMessageFormat(true);
        messageSource.setBasenames("classpath:messages", "classpath:validation");
        messageSource.setConcurrentRefresh(true);
        messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
        messageSource.setDefaultLocale(MessageUtil.locale);
        messageSource.setFallbackToSystemLocale(false);
        messageSource.setUseCodeAsDefaultMessage(true);
    }

    public static String getMessage(String code) {
        return getMessage(code, LocaleContextHolder.getLocale(), null);
    }

    public static String getMessage(Bundle bundle, String code) {
        return getMessage(bundle, code, LocaleContextHolder.getLocale(), (Object) null);
    }

    public static String getMessage(String code, Locale locale) {
        return getMessage(code, locale, (Object) null);
    }

    public static String getMessage(Bundle bundle, String code, Locale locale) {
        return getMessage(bundle, code, locale, (Object) null);
    }

    public static String getMessage(String code, Object... args) {
        return getMessage(Bundle.DEFAULT, code, LocaleContextHolder.getLocale(), args);
    }

    public static String getMessage(Bundle bundle, String code, Locale locale, Object... args) {
        String message;
        try {
            message = messageSource.getMessage(code, args, locale);
            if (args != null && args.length > 0) {
                message = MessageFormat.format(message, args);
            }
        } catch (Exception ex) {
            log.debug(ex.getMessage(), ex);
            message = code;
        }

        return message;
    }
}
