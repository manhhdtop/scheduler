package info.manhhdtop.rp.scheduler.utils;

import info.manhhdtop.rp.scheduler.dto.request.job.CreateJobRequest;
import info.manhhdtop.rp.scheduler.enums.JobType;
import info.manhhdtop.rp.scheduler.exception.InvalidException;
import info.manhhdtop.rp.scheduler.job.IJob;
import lombok.extern.slf4j.Slf4j;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.data.util.ReflectionUtils;

import java.util.Map;

@Slf4j
public class JobUtils {
    private static final String JOB_PACKAGE = "info.manhhdtop.rp.scheduler.job";

    public static JobDetail createJob(JobType type, String name) {
        return createJob(type, name, null);
    }

    public static JobDetail createJob(JobType type, String name, Map<String, Object> data) {
        var className = "%s.%sJob".formatted(JOB_PACKAGE, type.name);
        try {
            var job = (IJob) ReflectionUtils.createInstanceIfPresent(className, null);
            return job.createJob(name, data);
        } catch (Exception ex) {
            log.error("(createJob) Exception: {}", ex.getMessage(), ex);
        }
        return null;
    }

    public static Trigger createTrigger(JobDetail jobDetail, String cronExpression) {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName(), jobDetail.getKey().getGroup())
                .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
                .build();
    }

    public static boolean validate(CreateJobRequest createJobRequest) throws InvalidException {
        var className = "%s.%sJob".formatted(JOB_PACKAGE, createJobRequest.getType().name);
        var job = (IJob) ReflectionUtils.createInstanceIfPresent(className, null);
        job.validate(createJobRequest.getData());
        return true;
    }
}
