package info.manhhdtop.rp.scheduler.utils;

import info.manhhdtop.rp.scheduler.dto.response.Paging;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PageUtil {
    private static final int NUM_PAGE_SHOW = 5;

    public static List<Paging> pagination(int totalPages) {
        return pagination(totalPages, 0, NUM_PAGE_SHOW);
    }

    public static List<Paging> pagination(int totalPages, int currentPage) {
        return pagination(totalPages, currentPage, NUM_PAGE_SHOW);
    }

    public static List<Paging> pagination(int totalPages, int currentPage, int numPageShow) {
        var pages = new ArrayList<Paging>();
        if (totalPages <= 1 || totalPages <= numPageShow) {
            return pages;
        }
        if (numPageShow <= 0) {
            numPageShow = NUM_PAGE_SHOW;
        }
        if (currentPage < 0) {
            currentPage = 0;
        }
        Set<Paging> listPages;
        if (currentPage <= 3) {
            listPages = Stream.of(1, 2, 3, null, totalPages - 2, totalPages - 1)
                    .map(PageUtil::createPaging)
                    .collect(Collectors.toSet());
        } else {
            if (currentPage >= totalPages - 4) {
                listPages = Stream.of(1, 2, null, totalPages - 3, totalPages - 2, totalPages - 1)
                        .map(PageUtil::createPaging)
                        .collect(Collectors.toSet());
            } else {
                listPages = Stream.of(1, 2, null, currentPage - 1, currentPage, currentPage + 1, null, totalPages - 2, totalPages - 1)
                        .map(PageUtil::createPaging)
                        .collect(Collectors.toSet());
            }
        }
        pages.addAll(listPages);
        return pages;
    }

    private static Paging createPaging(Integer page) {
        return Paging.builder()
                .page(page)
                .name(page == null ? "..." : String.valueOf(page + 1))
                .url(page == null ? null : String.format("pageNumber=%d", page))
                .build();
    }
}
