package info.manhhdtop.rp.scheduler.utils.constant;

public class ConfigConstant {
    public static final String APPLICATION_INITIALIZATION = "APPLICATION_INITIALIZATION";
    public static final String DEFAULT_USER_GROUP = "DEFAULT_GROUP";
    public static final String DEFAULT_ADMIN_ROLE = "ROLE_ADMIN";
    public static final String DEFAULT_USER_ROLE = "ROLE_USER";
}
