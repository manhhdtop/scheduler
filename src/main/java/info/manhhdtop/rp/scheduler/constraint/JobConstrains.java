package info.manhhdtop.rp.scheduler.constraint;

import info.manhhdtop.rp.scheduler.constraint.validators.JobValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Constraint(validatedBy = JobValidator.class)
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface JobConstrains {

    String message() default "Validate failed on field data";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    boolean required() default true;
}
