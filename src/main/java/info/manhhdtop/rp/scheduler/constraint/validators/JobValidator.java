package info.manhhdtop.rp.scheduler.constraint.validators;

import info.manhhdtop.rp.scheduler.constraint.JobConstrains;
import info.manhhdtop.rp.scheduler.dto.request.job.CreateJobRequest;
import info.manhhdtop.rp.scheduler.exception.InvalidException;
import info.manhhdtop.rp.scheduler.utils.JobUtils;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.util.CollectionUtils;

public class JobValidator implements ConstraintValidator<JobConstrains, CreateJobRequest> {
    private boolean required;

    @Override
    public void initialize(JobConstrains jobConstrains) {
        ConstraintValidator.super.initialize(jobConstrains);
        this.required = jobConstrains.required();
    }

    @Override
    public boolean isValid(CreateJobRequest createJobRequest, ConstraintValidatorContext constraintValidatorContext) {
        if (required && CollectionUtils.isEmpty(createJobRequest.getData())) {
            return false;
        }
        try {
            return JobUtils.validate(createJobRequest);
        } catch (InvalidException ex) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(ex.getError())
                    .addPropertyNode("data")
                    .addConstraintViolation();
            return false;
        } catch (Exception ex) {
            return false;
        }
    }
}
