package info.manhhdtop.rp.scheduler.constraint.validators;

import info.manhhdtop.rp.scheduler.constraint.ValidCronExpression;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.quartz.CronExpression;
import org.springframework.util.StringUtils;

import java.text.ParseException;

public class CronExpressionValidator implements ConstraintValidator<ValidCronExpression, String> {
    private boolean required;

    @Override
    public void initialize(ValidCronExpression constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        this.required = constraintAnnotation.required();
    }

    @Override
    public boolean isValid(String cron, ConstraintValidatorContext constraintValidatorContext) {
        if (!StringUtils.hasText(cron)) {
            return !required;
        }
        try {
            new CronExpression(cron);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }
}
