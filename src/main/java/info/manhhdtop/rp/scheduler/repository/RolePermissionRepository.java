package info.manhhdtop.rp.scheduler.repository;

import info.manhhdtop.rp.scheduler.entity.RolePermissionEntity;
import info.manhhdtop.rp.scheduler.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface RolePermissionRepository extends BaseRepository<RolePermissionEntity> {
    List<RolePermissionEntity> findByRoleIdIn(Set<Long> roleIds);

    List<RolePermissionEntity> findByRoleIdAndPermissionIdIn(Long roleAdminId, Collection<Long> roleAdminPermissionIds);
}
