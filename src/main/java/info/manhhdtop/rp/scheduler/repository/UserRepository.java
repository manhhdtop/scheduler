package info.manhhdtop.rp.scheduler.repository;

import info.manhhdtop.rp.scheduler.entity.UserEntity;
import info.manhhdtop.rp.scheduler.repository.base.BaseRepository;
import info.manhhdtop.rp.scheduler.repository.custom.UserRepositoryCustom;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseRepository<UserEntity>, UserRepositoryCustom {
    Optional<UserEntity> findByUsername(String username);
}
