package info.manhhdtop.rp.scheduler.repository;

import info.manhhdtop.rp.scheduler.entity.GroupEntity;
import info.manhhdtop.rp.scheduler.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends BaseRepository<GroupEntity> {
}
