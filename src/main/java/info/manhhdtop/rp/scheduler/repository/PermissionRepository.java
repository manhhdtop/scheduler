package info.manhhdtop.rp.scheduler.repository;

import info.manhhdtop.rp.scheduler.entity.PermissionEntity;
import info.manhhdtop.rp.scheduler.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepository extends BaseRepository<PermissionEntity> {
    PermissionEntity findByCode(String code);
}
