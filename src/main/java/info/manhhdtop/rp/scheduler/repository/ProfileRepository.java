package info.manhhdtop.rp.scheduler.repository;

import info.manhhdtop.rp.scheduler.entity.ProfileEntity;
import info.manhhdtop.rp.scheduler.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends BaseRepository<ProfileEntity> {
    ProfileEntity findByUserId(Long userId);
}
