package info.manhhdtop.rp.scheduler.repository.custom;

import info.manhhdtop.rp.scheduler.dto.request.config.ConfigSearchRequest;
import info.manhhdtop.rp.scheduler.entity.ConfigEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ConfigRepositoryCustom {
    Page<ConfigEntity> searchConfig(ConfigSearchRequest request, Pageable pageable);
}
