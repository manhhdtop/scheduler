package info.manhhdtop.rp.scheduler.repository.custom.impl;

import info.manhhdtop.rp.scheduler.dto.request.config.ConfigSearchRequest;
import info.manhhdtop.rp.scheduler.entity.ConfigEntity;
import info.manhhdtop.rp.scheduler.repository.custom.ConfigRepositoryCustom;
import info.manhhdtop.rp.scheduler.utils.StringUtil;
import info.manhhdtop.rp.scheduler.utils.constant.ConfigConstant;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ConfigRepositoryCustomImpl implements ConfigRepositoryCustom {
    private final EntityManager entityManager;
    private final Class<ConfigEntity> entityClass;
    private final CriteriaBuilder criteriaBuilder;

    public ConfigRepositoryCustomImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.entityClass = ConfigEntity.class;
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    @Override
    public Page<ConfigEntity> searchConfig(ConfigSearchRequest request, Pageable pageable) {
        var criteriaQuery = this.criteriaBuilder.createQuery(entityClass);
        var itemRoot = criteriaQuery.from(entityClass);
        var predicate = getPredicate(request, itemRoot);
        var countQuery = this.criteriaBuilder.createQuery(Long.class);
        var countRoot = countQuery.from(entityClass);
        var countPredicate = getPredicate(request, countRoot);
        countQuery.where(countPredicate);
        countQuery.select(this.criteriaBuilder.count(countRoot));
        var count = entityManager.createQuery(countQuery).getSingleResult();
        if (count == 0) {
            return new PageImpl<>(new ArrayList<>(), pageable, count);
        }
        criteriaQuery.where(predicate);
        var query = entityManager.createQuery(criteriaQuery);
        query.setFirstResult((pageable.getPageNumber()) * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());
        var entities = query.getResultList();
        return new PageImpl<>(entities, pageable, count);
    }

    private Predicate getPredicate(ConfigSearchRequest request, Root<ConfigEntity> itemRoot) {
        List<Predicate> predicates = new ArrayList<>();
        if (request != null) {
            if (StringUtil.isNotBlank(request.getCode())) {
                predicates.add(criteriaBuilder.like(itemRoot.get("code"), "%" + request.getCode() + "%"));
            }
            if (StringUtil.isNotBlank(request.getName())) {
                predicates.add(criteriaBuilder.like(itemRoot.get("name"), "%" + request.getName() + "%"));
            }
        }
        predicates.add(criteriaBuilder.notEqual(itemRoot.get("code"), ConfigConstant.APPLICATION_INITIALIZATION));
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
