package info.manhhdtop.rp.scheduler.repository.custom;

public interface UserRepositoryCustom {
    boolean checkUserExist(String username, String email);
}
