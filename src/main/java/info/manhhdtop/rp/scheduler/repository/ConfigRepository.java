package info.manhhdtop.rp.scheduler.repository;

import info.manhhdtop.rp.scheduler.entity.ConfigEntity;
import info.manhhdtop.rp.scheduler.repository.base.BaseRepository;
import info.manhhdtop.rp.scheduler.repository.custom.ConfigRepositoryCustom;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigRepository extends BaseRepository<ConfigEntity>, ConfigRepositoryCustom {
    ConfigEntity findByCode(String code);
}
