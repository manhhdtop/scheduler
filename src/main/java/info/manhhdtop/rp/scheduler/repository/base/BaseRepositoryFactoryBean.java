package info.manhhdtop.rp.scheduler.repository.base;

import jakarta.persistence.EntityManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import java.io.Serializable;

public class BaseRepositoryFactoryBean<JR extends JpaRepository<T, ID>, T, ID extends Serializable> extends JpaRepositoryFactoryBean<JR, T, ID> {
    public BaseRepositoryFactoryBean(Class<? extends JR> repositoryInterface) {
        super(repositoryInterface);
    }

    @Override
    protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager) {
        return new BaseRepositoryFactory<T, ID>(entityManager);
    }
}