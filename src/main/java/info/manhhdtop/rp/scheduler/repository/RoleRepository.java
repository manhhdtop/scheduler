package info.manhhdtop.rp.scheduler.repository;

import info.manhhdtop.rp.scheduler.entity.RoleEntity;
import info.manhhdtop.rp.scheduler.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends BaseRepository<RoleEntity> {
    RoleEntity findByCode(String code);
}
