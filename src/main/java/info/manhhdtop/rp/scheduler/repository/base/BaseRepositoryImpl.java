package info.manhhdtop.rp.scheduler.repository.base;

import info.manhhdtop.rp.scheduler.entity.base.BaseModel;
import jakarta.persistence.EntityManager;
import lombok.Getter;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import java.util.Optional;

public class BaseRepositoryImpl<T> extends SimpleJpaRepository<T, Long> implements BaseRepository<T> {
    protected final EntityManager entityManager;
    @Getter
    private final Class<T> klass;

    BaseRepositoryImpl(JpaEntityInformation<T, Long> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
        this.klass = entityInformation.getJavaType();
    }

    @Override
    public void delete(Long id) {
        Optional<T> t = findById(id);
        t.ifPresent(e -> {
            ((BaseModel) e).setDeleted(true);
            super.save(e);
        });
    }
}
