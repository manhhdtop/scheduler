package info.manhhdtop.rp.scheduler.repository;

import info.manhhdtop.rp.scheduler.entity.UserRoleEntity;
import info.manhhdtop.rp.scheduler.repository.base.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends BaseRepository<UserRoleEntity> {
    List<UserRoleEntity> findByUserId(Long id);

    UserRoleEntity findByUserIdAndRoleId(Long userId, Long roleId);
}
