package info.manhhdtop.rp.scheduler.repository.custom.impl;

import info.manhhdtop.rp.scheduler.repository.custom.UserRepositoryCustom;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository
public class UserRepositoryCustomImpl implements UserRepositoryCustom {
    private final EntityManager entityManager;

    @Override
    public boolean checkUserExist(String username, String email) {
        var query = entityManager.createNativeQuery("SELECT COUNT(id) FROM user WHERE username = :username OR email = :email");
        query.setParameter("username", username);
        query.setParameter("email", email);
        var result = (Long) query.getSingleResult();
        return result != 0;
    }
}
