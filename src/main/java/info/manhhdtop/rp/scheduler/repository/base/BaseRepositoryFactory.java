package info.manhhdtop.rp.scheduler.repository.base;

import info.manhhdtop.rp.scheduler.utils.AssertUtil;
import jakarta.persistence.EntityManager;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;

import java.io.Serializable;

public class BaseRepositoryFactory<T, ID extends Serializable> extends JpaRepositoryFactory {
    private final EntityManager entityManager;

    public BaseRepositoryFactory(EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    @Override
    protected JpaRepositoryImplementation<T, ID> getTargetRepository(RepositoryInformation information, EntityManager entityManager) {
        JpaEntityInformation<?, Serializable> entityInformation = this.getEntityInformation(information.getDomainType());
        var repository = this.getTargetRepositoryViaReflection(information, entityInformation, entityManager);
        AssertUtil.isInstanceOf(BaseRepositoryImpl.class, repository);
        return (JpaRepositoryImplementation<T, ID>) repository;
    }

    @Override
    protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
        return BaseRepositoryImpl.class;
    }
}