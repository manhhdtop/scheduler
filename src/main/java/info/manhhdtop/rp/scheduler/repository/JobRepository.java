package info.manhhdtop.rp.scheduler.repository;

import info.manhhdtop.rp.scheduler.entity.JobEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository extends JpaRepository<JobEntity, Long> {
    JobEntity findByName(String name);
}
